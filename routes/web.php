<?php

/*
|--------------------------------------------------------------------------
| AUTH
|--------------------------------------------------------------------------
*/
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| FRONT
|--------------------------------------------------------------------------
*/
/* Main page */
Route::get('/', 'Front\MainPageController@index')->name('main_page');

/* Number */
Route::get('/number/{slug}', 'Front\NumberController@show')->name('number.show');

/* Static pages */
Route::get('/about', 'Front\StaticPageController@about')->name('static_page.about');
Route::get('/contacts', 'Front\StaticPageController@contacts')->name('static_page.contacts');

/* Gallery */
Route::get('/gallery', 'Front\GalleryController@show')->name('gallery.show');

/* Feedback */
Route::post('/feedback-store', 'Front\FeedbackController@store')->name('feedback.store');

/* Order */
Route::post('/reserve', 'Front\OrderController@reserve')->name('order.reserve');
/*
|--------------------------------------------------------------------------
| ADMIN
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    /* Orders */
    Route::get('/', 'Admin\OrderController@index')->name('admin.orders.index');
    Route::resource('orders','Admin\OrderController')->except(['index']);

    /* Numbers */
    Route::resource('numbers','Admin\NumberController');
    Route::post('/number-save-image', 'Admin\NumberController@saveImage')->name('admin.number_save_image');
    Route::post('/number-sort-images', 'Admin\NumberController@sortImages')->name('admin.number_sort_images');
    Route::post('/number-remove-image', 'Admin\NumberController@removeImage')->name('admin.number_image_remove_image');
    Route::post('/number-image-switch-active', 'Admin\NumberController@imageSwitchActive')->name('admin.number_image_switch_active');
    Route::post('/number-add-price', 'Admin\NumberController@addPrice')->name('admin.number_add_price');
    Route::post('/number-remove-price', 'Admin\NumberController@removePrice')->name('admin.number_add_price');
    Route::post('/number-switch-active', 'Admin\NumberController@switchActive')->name('admin.number_switch_active');
    Route::post('/numbers-sort', 'Admin\NumberController@sortNumbers')->name('admin.numbers_sort');

    /* Slider */
    Route::get('/sliders', 'Admin\SliderController@index')->name('admin.sliders');
    Route::post('/slider-save-image', 'Admin\SliderController@saveImage')->name('admin.slider_save_image');
    Route::post('/slider-sort-images', 'Admin\SliderController@sortImages')->name('admin.slider_sort_images');
    Route::post('/slider-remove-image', 'Admin\SliderController@removeImage')->name('admin.slider_remove_image');
    Route::post('/slider-switch-active', 'Admin\SliderController@switchActive')->name('admin.slider_switch_active');

    /* Static pages */
    Route::get('/static-pages/{slug}', 'Admin\StaticPageController@edit')->name('admin.static_pages_edit');
    Route::put('/static-pages/{slug}', 'Admin\StaticPageController@update')->name('admin.static_pages_update');

    /* Gallery */
    Route::get('/gallery', 'Admin\GalleryController@index')->name('admin.gallery.show');
    Route::post('/gallery-save-image', 'Admin\GalleryController@saveImage')->name('admin.gallery_save_image');
    Route::post('/gallery-sort-images', 'Admin\GalleryController@sortImages')->name('admin.gallery_sort_images');
    Route::post('/gallery-remove-image', 'Admin\GalleryController@removeImage')->name('admin.gallery_remove_image');
    Route::post('/gallery-image-switch-active', 'Admin\GalleryController@switchActive')->name('admin.gallery_switch_active');
    Route::post('/gallery-enable-numbers-photo', 'Admin\GalleryController@enableNumbersPhoto')->name('admin.gallery_enable_numbers_photo');

    /* Info */
    Route::resource('info', 'Admin\InfoController');

    /* Text */
    Route::resource('texts', 'Admin\TextController');

    /* Feedback */
    Route::get('/feedback', 'Admin\FeedbackController@index')->name('admin.feedback.show');
});
