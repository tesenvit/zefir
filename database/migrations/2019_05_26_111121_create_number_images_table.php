<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumberImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('number_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('number_id');
            $table->foreign('number_id')->references('id')->on('numbers');
            $table->string('path');
            $table->string('thumbnail');
            $table->integer('sort');
            $table->tinyInteger('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('number_images');
    }
}
