<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumberCosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('number_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('number_id');
            $table->foreign('number_id')->references('id')->on('numbers');
            $table->dateTime('from')->nullable();
            $table->dateTime('to')->nullable();
            $table->integer('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('number_costs');
    }
}
