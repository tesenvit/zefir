<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.4.0.min.js') }}"></script>
    <script src="{{ asset('js/datepicker.js') }}" defer></script>
    <script src="{{ asset('js/owl.carousel.2.3.4.min.js') }}" defer></script>
    <script src="{{ asset('js/fancybox.min.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css?v=1.2') }}" rel="stylesheet">
    <link href="{{ asset('css/custom/main.css?v=2.0') }}" rel="stylesheet">
    <link href="{{ asset('css/custom/front.css?v=2.6') }}" rel="stylesheet">
    <link href="{{ asset('css/datepicker.css?v=1') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fancybox.min.css') }}" rel="stylesheet">

    <!-- Check success modal -->
    <script>
        var successModal = false,
            bookingFormModal = false;
    </script>

    @if(session()->has('bookingModalCheck') && session('bookingModalCheck') == 1)
        <script>
            bookingFormModal = true;
        </script>
    @endif

    @if(session()->has('successModal'))
        <script>
            successModal = "{{ session('successModal') }}";
        </script>
    @endif


</head>

<body>
    <div id="app">

        <!-- Navigation -->
        @include('layouts.front.navigation')

        <!-- Content -->
        <main class="content">
            @yield('content')
        </main>

        <!-- Footer -->
        @include('layouts.front.footer')
    </div>

    <!-- Widgets -->
    @include('front.widgets.nav_phones')
    @include('front.widgets.nav_booking')
    @include('front.widgets.static_page_rules')
    @include('front.widgets.success_modal')

    <script src="{{ asset('js/custom/main.js?v=1.0') }}" defer></script>
    <script src="{{ asset('js/custom/front.js?v=1.4') }}" defer></script>
</body>

</html>
