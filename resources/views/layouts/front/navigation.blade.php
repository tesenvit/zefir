<nav class="navbar navbar-light navbar-expand-md bg-faded justify-content-center p-0 menu-header fixed-top">
    <div class="container nav-container">
        <!-- Logo -->
        <a href="{{ Request::is('/') ? '#' : route('main_page') }}" class="navbar-brand d-flex mr-auto navbar-brand-logo">
            <img class="logo" src="{{ asset('storage/img/z-logo-bk.png?v=1') }}" alt="logo">
        </a>

        <!-- Btn phone for mobile device -->
        <button class="nav-link btn btn-sm btn-outline-dark  menu-header-phone-btn-mobile" data-toggle="modal" data-target="#nav-phones">
            <i class="fas fa-phone"></i>
        </button>

        <!-- Btn booking for mobile device -->
        <button class="nav-link btn btn-sm btn-outline-dark menu-header-booking-btn-mobile" data-toggle="modal" data-target="#nav-booking">
            <span class="booking-laptop-text">Забронировать</span>
            <i class="fas fa-key booking-mobile-text"></i>
        </button>

        <!-- Hamburger -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbarMenu">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Navbar-->
        <div class="navbar-collapse collapse w-100 text-lefcentert" id="collapsingNavbarMenu">
            <ul class="navbar-nav w-100 justify-content- main-menu">
                <li class="nav-item dropdown navigation-dropdown mx-lg-3 mx-md-2 ">
                    <a id="front-navbar-dropdown"
                       class="nav-link dropdown-toggle px-3 nav-menu-link-dropdown"
                       href="#"
                       role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       style="width: 112px"
                    >
                        <span class="nav-dropdown-title {{ Request::is('number/*') ? 'navigation-dropdown-active' : '' }}">
                            <span class="caret">Номера</span>
                        </span>
                    </a>

                    <div class="dropdown-menu navigation-dropdown-menu pl-2 pr-4" style="left:-48px" aria-labelledby="front-navbar-dropdown">
                        @foreach($allNumbers as $number)
                            <a href="{{ route('number.show', [$number->slug]) }}"
                               class="nav-link nav-link-dropdown text-dark {{ (request()->path() == 'number/' . $number->slug) ? 'nav-link-dropdown-active' : '' }}">
                                <i class="fas fa-bed section-name-icon mr-3"></i>
                                <span>{{ $number->title }}</span>
                            </a>
                        @endforeach
                    </div>
                </li>

                <li class="nav-item mx-lg-3 mx-md-2">
                    <a class="nav-link px-3 nav-menu-link" href="{{ route('gallery.show') }}">
                        <span class="{{ Request::is('gallery') ? 'nav-link-active' : '' }}">Галерея</span>
                    </a>
                </li>

                <li class="nav-item mx-lg-3 mx-md-2">
                    <a class="nav-link px-3 nav-menu-link" href="{{ route('static_page.about') }}">
                        <span class="{{ Request::is('about') ? 'nav-link-active' : '' }}">О&nbsp;нас</span>
                    </a>
                </li>

                <li class="nav-item mx-lg-3 mx-md-2">
                    <a class="nav-link px-3 nav-menu-link" href="{{ route('static_page.contacts') }}">
                        <span class="{{ Request::is('contacts') ? 'nav-link-active' : '' }}">Контакты</span>
                    </a>
                </li>
            </ul>

            <ul class="nav navbar-nav w-100 justify-content-end menu-header-booking">
                <li class="nav-item pr-2">
                    <button class="nav-link btn btn-sm btn-outline-dark menu-header-phone-btn-desktop" data-toggle="modal" data-target="#nav-phones">
                        <i class="fas fa-phone"></i>
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link btn btn-sm btn-outline-dark menu-header-booking-btn" data-toggle="modal" data-target="#nav-booking">
                        <span class="menu-header-booking-btn-text">Забронировать</span>
                        <i class="fas fa-key menu-header-booking-btn-key"></i>
                    </button>
                </li>
            </ul>
        </div>

    </div>
</nav>
