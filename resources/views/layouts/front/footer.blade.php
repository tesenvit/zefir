<footer class="footer">
    <div class="container footer-container">
        <div class="row mb-md-5 mb-sm-2">
            <div class="col-lg-4 col-md-4 col-sm-6 px-md-0 px-sm-3 pt-md-2 text-md-left text-sm-left container-logo">
                <a href="{{ Request::is('/') ? '#' : route('main_page') }}">
                    <img class="footer-logo" src="{{ asset('storage/img/z-logo-wh.png?v=1') }}" alt="logo">
                </a>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 text-md-center text-sm-right px-md-0 px-sm-3 footer-phones">
                @foreach(information('phones') as $phone)
                    <div>
                        <i class="fas fa-phone mr-2"></i>
                        <a href="tel:{{ $phone }}">{{ $phone }}</a>
                    </div>
                @endforeach
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 text-md-right text-sm-center container-address">
                <p>Украина, Одесская область, пгт. Затока, улица Лиманная, участок 307</p>
            </div>
        </div>

        <div class="row">
            <div class="offset-lg-4 offset-md-0 col-lg-4 col-md-6 col-sm-6 footer-email text-lg-center ">
                <i class="far fa-envelope mr-2"></i>
                <span><a href="mailto:{{ information('email') }}">{{ information('email') }}</a></span>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 text-md-right text-sm-right container-social">
                <a href="https://www.instagram.com/zefirzatoka/?igshid=mb6kvi9ibnjw" target="_blank">
                    <img src="{{ asset('storage/img/insta.png') }}" alt="instagram" width="35">
                </a>
                <a href="https://www.booking.com/hotel/ua/mini-otel-zefir.ru.html" target="_blank">
                    <img src="{{ asset('storage/img/booking.png') }}" alt="booking" width="35">
                </a>
            </div>
        </div>
    </div>
</footer>
