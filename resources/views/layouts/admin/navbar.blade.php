<nav class="navbar navbar-expand-md navbar-light admin-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('admin.orders.index') }}">
            Администратор
        </a>
        <ul class="navbar-nav ml-auto admin-navbar-right-side">
            <li class="nav-item dropdown">
                <a id="admin-navbar-dropdown" class="nav-link dropdown-toggle text-violet" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="fas fa-user"></i> <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="admin-navbar-dropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Выход
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>
