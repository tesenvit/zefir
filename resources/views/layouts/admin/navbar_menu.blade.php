<nav class="navbar navbar-expand-md navbar-light admin-navbar-menu">
    <div class="container">

        <h5 class="admin-navbar-menu-text">Меню</h5>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#admin-navbar-menu-collapse" aria-controls="admin-navbar-menu-collapse" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="admin-navbar-menu-collapse">
            <ul class="nav navbar-nav mx-auto nav-fill w-100 ">

                <li class="nav-item">
                    <a class="nav-link {{ (Request::is('admin/numbers') || (Request::is('admin/numbers/*'))) ? 'admin-nav-link-active' : '' }}" href="{{ route('numbers.index') }}">
                        <span class="nav-link-text">Номера</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/gallery') ? 'admin-nav-link-active' : '' }}" href="{{ route('admin.gallery.show') }}">
                        <span class="nav-link-text">Галерея</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/sliders') ? 'admin-nav-link-active' : '' }}" href="{{ route('admin.sliders') }}">
                        <span class="nav-link-text">Слайдер</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/feedback') ? 'admin-nav-link-active' : '' }}" href="{{ route('admin.feedback.show') }}">
                        <span class="nav-link-text">
                            Обратная связь {{ (feedback_count() > 0) ? '(' . feedback_count() . ')' : '' }}
                        </span>
                    </a>
                </li>

                <li class="nav-item dropdown navigation-dropdown">
                    <a id="front-navbar-dropdown" class="nav-link dropdown-toggle px-3 nav-menu-link-dropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <span class="nav-dropdown-title">
                            <span class="caret">
                                <span class="{{ Request::is('admin/static-pages/*') ? 'admin-nav-link-active' : '' }}">
                                    Страницы
                                </span>
                            </span>
                        </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="front-navbar-dropdown">
                        @foreach($adminStaticPages as $page)
                            <a href="{{ route('admin.static_pages_edit', [$page->slug]) }}" class="nav-link px-3 admin-menu-dropdown-link">
                                <span class="{{ (request()->path() == 'admin/static-pages/' . $page->slug) ? 'admin-nav-link-active' : '' }}">
                                    {{ $page->title }}
                                </span>
                            </a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown navigation-dropdown">
                    <a id="front-navbar-dropdown" class="nav-link dropdown-toggle px-3 nav-menu-link-dropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <span class="nav-dropdown-title">
                            <span class="caret">
                                <span class="{{ (Request::is('admin/info') || Request::is('admin/info/*')) || Request::is('admin/texts') || Request::is('admin/texts/*') ? 'admin-nav-link-active' : '' }}">
                                     Инфо
                                </span>
                            </span>
                        </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="front-navbar-dropdown">
                        <a href="{{ route('info.index') }}" class="nav-link px-3 admin-menu-dropdown-link {{ Request::is('admin/info') || Request::is('admin/info/*') ? 'admin-nav-link-active' : '' }}">
                            Общая информация
                        </a>
                        <a href="{{ route('texts.index') }}" class="nav-link px-3 admin-menu-dropdown-link {{ Request::is('admin/texts') || Request::is('admin/texts/*') ? 'admin-nav-link-active' : '' }}">
                            Тексты
                        </a>
                    </div>
                </li>

            </ul>

        </div>

    </div>
</nav>
