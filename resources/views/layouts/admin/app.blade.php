<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.4.0.min.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="{{ asset('js/cropper.min.js') }}" defer></script>
    <script src="{{ asset('js/jquery-ui.js') }}" defer></script>
    <script src="{{ asset('js/datepicker.js') }}" defer></script>
{{--    <script src="{{ asset('js/ckeditor.js') }}" defer></script>--}}
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css?v=1.2') }}" rel="stylesheet">
    <link href="{{ asset('css/custom/main.css?v=1.6') }}" rel="stylesheet">
    <link href="{{ asset('css/custom/admin.css?v=2.0') }}" rel="stylesheet">

    <link href="{{ asset('css/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datepicker.css?v=1') }}" rel="stylesheet">


{{--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.css"/>--}}
{{--    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js" defer></script>--}}


</head>

<body>
    <div id="app">

        <!-- Navigation -->
        @include('layouts.admin.navbar')
        @include('layouts.admin.navbar_menu')

        <!-- Content -->
        <main class="content">
            @yield('content')
        </main>

    </div>

    <script src="{{ asset('js/custom/main.js?v=1.1') }}" defer></script>
    <script src="{{ asset('js/custom/admin.js?v=1.8') }}" defer></script>

</body>

</html>
