@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <br>

        <h3 class="text-center">Галерея</h3>

        <hr>

        <div class="gallery-enable-numbers-photo-box">
            @include('admin.gallery.enable_numbers_photo')
        </div>

        <hr>

        <div class="row">
            <div class="col-lg-12">
                <form class="form-inline">
                    <div class="form-group mr-2">
                        <label for="gallery-photo-file">Добавить изображение</label>
                    </div>
                    <div class="form-group mb-2">
                        <input type="file" class="form-control-file" name="gallery-photo" id="gallery-photo-file">
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-outline-primary btn-cropper-modal" data-toggle="modal" data-target="#gallery-cropper-modal">
                            Открыть CROPPER
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <hr>

        <div class="row sortable-gallery-images">
            @include('admin.gallery.list_images')
        </div>

    </div>

    @include('admin.widgets.cropper_modal', ['id' => 'gallery-cropper-modal', 'btnClass' => 'crop-photo-for-gallery'])

@endsection