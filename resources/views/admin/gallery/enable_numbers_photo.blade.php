<span class="pr-3">Вкл/Выкл отображение фото номеров</span>

@if(settings('gallery_enable_numbers_photo'))
    <i class="fas fa-toggle-on inactive fa-2x gallery-enable-numbers-photo text-success align-middle"></i>
@else
    <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x gallery-enable-numbers-photo text-danger align-middle"></i>
@endif