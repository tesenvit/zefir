@extends('layouts.admin.app')

@section('content')
    <br>
    <h3 class="text-center">Тексты</h3>
    <br>

    <div class="container">

        @include('admin.widgets.success_message')

        <a href="{{ route('texts.create') }}" class="btn btn-outline-primary">Создать</a>

        <br>
        <br>

        <table class="table table-hover">
            <thead>
            <tr class="text-center">
                <th scope="col">Название</th>
                <th scope="col">Ключ</th>
                <th scope="col">Значение</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($texts as $text)
                <tr class="text-center">
                    <td>{{ $text->title }}</td>
                    <td>{{ $text->key }}</td>
                    <td>{!! $text->value !!}</td>
                    <td class="text-primary">
                        <a href="{{ route('texts.edit', ['id' => $text->id]) }}">
                            <i class="fas fa-edit fa-lg"></i>
                        </a>
                    </td>
                    <td class="text-danger">
                        <form action="{{ route('texts.destroy', [$text->id]) }}" method="POST" class="text-remove-form">
                            @csrf
                            @method('DELETE')
                            <i class="far fa-trash-alt fa-lg text-remove"></i>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection