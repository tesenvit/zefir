@extends('layouts.admin.app')

@section('content')
    <br>
    <h3 class="text-center">Создание текста</h3>
    <br>

    <div class="container">

        @include('admin.widgets.success_message')

        <form action="{{ route('texts.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="form-group">
                <label for="text-title">Название</label>
                <input type="text"
                       class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                       id="text-title"
                       name="title"
                       required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="text-key">Ключ</label>
                <input type="text"
                       class="form-control {{ $errors->has('key') ? ' is-invalid' : '' }}"
                       id="text-key"
                       name="key"
                       required>

                @if ($errors->has('key'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('key') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="text-value">Значение</label>

                <textarea class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}"
                          name="value"
                          id="text-value"
                          rows="6"
                          placeholder="Значение"
                          required></textarea>

                @if ($errors->has('value'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('value') }}</strong>
                    </span>
                @endif
            </div>

            <br>

            <button type="submit" class="btn btn-outline-primary">Сохранить</button>

        </form>
    </div>
@endsection