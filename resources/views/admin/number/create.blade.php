@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <br>
        <h3 class="text-center">
            Создать номер
        </h3>
        <br>

        <form action="{{ route('numbers.store') }}" method="POST" novalidate>
            @csrf
            @method('POST')
            <div class="form-group">
                <label for="number-title">Название</label>
                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="number-title" name="title"  placeholder="Название" value="{{ old('title') }}" required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="number-preview">Превью</label>
                <input type="text" class="form-control{{ $errors->has('preview') ? ' is-invalid' : '' }}" id="number-preview" name="preview"  placeholder="Превью" value="{{ old('preview') }}" required>

                @if ($errors->has('preview'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('preview') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="number-description">Описание</label>
                <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="number-description" rows="3" placeholder="Описание" required>{{ old('description') }}</textarea>

                @if ($errors->has('description'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="pt-1 pb-2">
                <small><i>*Цены и фотографии добавляются при редактировании</i></small>
            </div>

            <div class="form-group pt-2">
                <button type="submit" class="btn btn-primary">Создать</button>
            </div>

        </form>
    </div>
@endsection