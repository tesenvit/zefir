@foreach($numbers as $number)
    <tr class="number-box-data" data-id="{{ $number->id }}">
        <th>{{ $number->title }}</th>
        <td>
            @if($number->getPrice())
                {{ $number->getPrice() }} грн.
            @else
                <span class="text-danger">Не заполнено</span>
            @endif
        </td>
        <td>
            @if($number->active)
                <i class="fas fa-toggle-on inactive fa-2x text-success number-switch-active"></i>
            @else
                <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x text-danger number-switch-active"></i>
            @endif
        </td>
        <td class="text-primary">
            <a href="{{ route('numbers.edit', ['id' => $number->id]) }}">
                <i class="fas fa-edit fa-lg"></i>
            </a>
        </td>
        <td class="text-danger">
            <form action="{{ route('numbers.destroy', [$number->id]) }}" method="POST" class="number-remove-form">
                @csrf
                @method('DELETE')
                <i class="far fa-trash-alt fa-lg number-remove"></i>
            </form>
        </td>
    </tr>
@endforeach