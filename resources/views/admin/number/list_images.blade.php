@if(!empty($number))
    @foreach($number->images->sortBy('sort') as $image)
        <div class="col-lg-3 py-3">
            <div class="card" data-id="{{ $image->id }}">

                <img class="card-img-top" src="{{ asset('storage/numbers/thumbnails/' . $image->thumbnail) }}" alt="slide">

                <div class="card-body px-2 pb-1 pt-2">
                    <div class="row">
                        <div class="col-lg-6 text-left">
                            @if($image->active)
                                <i class="fas fa-toggle-on inactive fa-2x number-image-switch-active text-success"></i>
                            @else
                                <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x number-image-switch-active text-danger"></i>
                            @endif
                        </div>
                        <div class="col-lg-6 text-right text-danger">
                            <i class="far fa-trash-alt number-image-remove"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
