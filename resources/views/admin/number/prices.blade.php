@if($number->cost)
    @foreach($number->cost as $cost)
        @php
            $date = '';

            if($cost->from !==null && $cost->to !==null)
                $date = $cost->from->format('d.m.Y') . '-' . $cost->to->format('d.m.Y');

        @endphp

        <div class="form-row number-box-price" data-id="{{ $cost->id }}">

            <div class="form-group col-lg-4 col-md-6 col-sm-7 col-8">
                <input type="text" class="form-control number-cost-date {{ $errors->has('dates' . $cost->id) || $errors->has('dates') ? 'is-invalid' : '' }}"
                       name="dates[{{ $cost->id }}]"
                       value="{{ old('dates.' . $cost->id) ?? $date }}"
                       required>

                @if ($errors->has('dates.'.$cost->id))
                    <span class="invalid-feedback" role="alert" style="display: block">
                    <strong>{{ $errors->first('dates.'.$cost->id) }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-lg-2 col-md-3 col-sm-4 col-3">
                <input type="text"
                       class="form-control number-cost-price {{ $errors->has('prices.'.$cost->id ) ? 'is-invalid' : '' }}"
                       name="prices[{{ $cost->id }}]"
                       value="{{ old('prices.' . $cost->id) ?? $cost->price }}"
                       required>

                @if ($errors->has('prices.'.$cost->id))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('prices.'.$cost->id) }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-lg-1 col-md-1 col-sm-1 col-1 text-danger">
                <i class="far fa-trash-alt number-price-remove align-bottom"></i>
            </div>
        </div>
    @endforeach
@endif



@if($errors->has('dates'))
    <span class="invalid-feedback" role="alert" style="display: block">
        <strong>{{ $errors->first('dates') }}</strong>
    </span>
@endif