@if(isset($numberCost))
    <div class="form-row number-box-price" data-id="{{ $numberCost->id }}">
        <div class="form-group col-lg-4 col-md-6 col-sm-7 col-8">
            <input type="text" class="form-control number-cost-date"
                   name="dates[{{ $numberCost->id }}]"
                   value="">
        </div>

        <div class="form-group col-lg-2 col-md-3 col-sm-4 col-3">
            <input type="text" class="form-control number-cost-price" name="prices[{{ $numberCost->id }}]" value="">
        </div>

        <div class="form-group col-lg-1 col-md-1 col-sm-1 col-1 text-danger">
            <i class="far fa-trash-alt number-price-remove align-bottom"></i>
        </div>
    </div>
@endif

<script src="{{ asset('js/custom/dp_adm_number_new_price.js') }}" defer></script>