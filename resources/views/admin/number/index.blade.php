@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <br>
        <h3 class="text-center">Номера</h3>
        <br>

        <a href="{{ route('numbers.create') }}" class="btn btn-outline-primary">
            <i class="fas fa-plus"></i>&nbsp;Добавить
        </a>
        <br>
        <br>

        @include('admin.widgets.success_message')

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Цена</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>

            <tbody class="list-numbers-body">
                @include('admin.number.list_numbers')
            </tbody>
        </table>
    </div>
@endsection