@extends('layouts.admin.app')

@section('content')
    <div class="container container-number" data-id="{{ $number->id }}">
        <br>
        <h3 class="text-center">Редактирование номера "{{ $number->title }}"</h3>
        <br>

        @include('admin.widgets.success_message')

        <form action="{{ route('numbers.update', ['id' => $number->id]) }}" method="POST" class="mb-5" enctype="multipart/form-data">

            @csrf
            @method('PUT')

            <!-- TITLE -->
            <div class="form-group">
                <label for="number-title">Название</label>
                <input type="text"
                       class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                       id="number-title"
                       name="title"
                       placeholder="Название" value="{{ old('title') ?? $number->title }}" required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <!-- URL -->
            <div class="form-group">
                <label for="number-url">URL</label>
                <input type="text"
                       class="form-control"
                       id="number-url"
                       name="slug"
                       value="{{ $number->slug }}" disabled>
            </div>

            <!-- PREVIEW -->
            <div class="form-group">
                <label for="number-preview">Превью</label>
                <input type="text"
                       class="form-control{{ $errors->has('preview') ? ' is-invalid' : '' }}"
                       id="number-preview"
                       name="preview"
                       placeholder="Превью"
                       value="{{ old('preview') ?? $number->preview }}" required>

                @if ($errors->has('preview'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('preview') }}</strong>
                    </span>
                @endif
            </div>

            <!-- DESCRIPTION -->
            <div class="form-group pb-3">
                <label for="number-description">Описание</label>
                <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                          name="description"
                          id="number-description"
                          rows="6"
                          placeholder="Описание"
                          required>{{ old('description') ?? $number->description }}</textarea>

                @if ($errors->has('description'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <hr>

            <!-- DATE -->
            <!-- Add date btn -->
            <div class="form-row pb-3">
                <button class="btn btn-outline-primary btn-sm number-add-price" type="button">
                    <i class="fas fa-plus"></i>
                    &nbsp;
                    <span>Добавить</span>
                </button>
            </div>

           <!-- labels -->
            <div class="form-row">
                <div class="form-group col-md-4 mb-0">
                    <label for="inputCity">Дата</label>
                </div>
                <div class="form-group col-md-2 mb-0">
                    <label for="inputCity">Цена</label>
                </div>
            </div>

            <!-- inputs -->
            <div class="number-prices">
                @include('admin.number.prices')
            </div>

            <hr>

            <!-- ADD IMAGE -->
            <div class="form-group mr-2 mb-0">
                <label for="number-photo-file">Добавить изображение</label>
            </div>

            <div class="form-row">
                <div class="form-group col-lg-4">
                    <input type="file" class="form-control-file" name="number-photo" id="number-photo-file">
                </div>
                <div class="form-group col-lg-6">
                    <button type="button" class="btn btn-outline-primary btn-cropper-modal" data-toggle="modal" data-target="#number-cropper-modal">
                        Открыть CROPPER
                    </button>
                </div>
            </div>

            <div class="row sortable-number-images">
                @include('admin.number.list_images')
            </div>

            <!-- SUBMIT -->
            <div class="form-group pt-2">
                <button type="submit" class="btn btn-primary">Обновить</button>
            </div>

        </form>
    </div>

    @include('admin.widgets.cropper_modal', ['id' => 'number-cropper-modal', 'btnClass' => 'crop-image-for-number'])

@endsection
