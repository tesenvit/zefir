@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <br>
        <h3 class="text-center">Обратная связь</h3>
        <br>

        @include('admin.widgets.success_message')

        <table class="table table-hover text-center">
            <thead>
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Email</th>
                <th scope="col">Сообщение</th>
            </tr>
            </thead>

            <tbody>
                @foreach($feedback as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->text }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection