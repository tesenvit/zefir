@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <br>
        <h3 class="text-center">Слайдер</h3>
        <br>

        <ul class="nav nav-tabs" id="sliderTabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="desktop-tab" data-toggle="tab" href="#desktop" role="tab" aria-controls="desktop" aria-selected="true" data-type="desktop">
                    Основной
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="mobile-tab" data-toggle="tab" href="#mobile" role="tab" aria-controls="mobile" aria-selected="false" data-type="mobile">
                    Мобильный
                </a>
            </li>
        </ul>

        <br>

        <div class="row">
            <div class="col-lg-12">
                <form class="form-inline">
                    <div class="form-group mr-2">
                        <label for="slider-photo-file">Добавить изображение</label>
                    </div>
                    <div class="form-group mb-2">
                        <input type="file" class="form-control-file" name="slider-photo" id="slider-photo-file">
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-outline-primary btn-cropper-modal" data-toggle="modal" data-target="#slider-cropper-modal">
                            Открыть CROPPER
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="tab-content list-slides" id="sliders-tab-content">
            @include('admin.sliders.list_slides')
        </div>

    </div>

    @include('admin.widgets.cropper_modal', ['id' => 'slider-cropper-modal', 'btnClass' => 'crop-photo-for-slider'])

@endsection

