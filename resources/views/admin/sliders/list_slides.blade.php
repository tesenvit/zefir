<!-- DESKTOP -->
<div class="tab-pane fade {{ (!isset($type) || isset($type) && $type === 'desktop') ? 'show active' : '' }}" id="desktop" role="tabpanel" aria-labelledby="desktop-tab">
    <hr>
    @include('admin.widgets.success_alerts_photo')
    @if(!empty($slides))
        <div class="row sortable-desktop">
            @foreach($slides as $slide)
                <div class="col-lg-3 py-3">
                    <div class="card" data-id="{{ $slide->id }}">

                        <img class="card-img-top" src="{{ '/storage/slider/' . $slide->path }}" alt="slide">

                        <div class="card-body px-2 pb-1 pt-2">
                            <div class="row">
                                <div class="col-lg-6 text-left">
                                    @if($slide->active)
                                        <i class="fas fa-toggle-on inactive fa-2x slide-switch-active text-success"></i>
                                    @else
                                        <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x slide-switch-active text-danger"></i>
                                    @endif
                                </div>
                                <div class="col-lg-6 text-right text-danger">
                                    <i class="far fa-trash-alt slide-remove"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>

<!-- MOBILE -->
<div class="tab-pane fade {{ (isset($type) && $type === 'mobile') ? 'show active' : '' }}" id="mobile" role="tabpanel" aria-labelledby="mobile-tab">
    <hr>
    @include('admin.widgets.success_alerts_photo')
    @if(!empty($slidesMobile))
        <div class="row sortable-mobile">
            @foreach($slidesMobile as $slide)
                <div class="col-lg-3 py-3">
                    <div class="card" data-id="{{ $slide->id }}">

                        <img class="card-img-top" src="{{ '/storage/slider/' . $slide->path }}" alt="slide">

                        <div class="card-body px-2 pb-1 pt-2">
                            <div class="row">
                                <div class="col-lg-6 text-left">
                                    @if($slide->active)
                                        <i class="fas fa-toggle-on inactive fa-2x slide-switch-active text-success"></i>
                                    @else
                                        <i class="fas fa-toggle-on fa-rotate-180 inactive fa-2x slide-switch-active text-danger"></i>
                                    @endif
                                </div>
                                <div class="col-lg-6 text-right text-danger">
                                    <i class="far fa-trash-alt slide-remove"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>


<script src="{{ asset('js/custom/sortable.js') }}" defer></script>
