@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <br>
        <h2 class="text-center">Страница "{{ $page->title }}"</h2>

        @include('admin.widgets.success_message')

        <form action="{{ route('admin.static_pages_update', [$page->slug]) }}" method="POST" novalidate>
            @csrf
            @method('PUT')

            <!-- TITLE -->
            <div class="form-group">
                <label for="static-page-title">Название</label>
                <input type="text"
                       class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                       id="static-page-title"
                       name="title"
                       placeholder="Название" value="{{ old('title') ?? $page->title }}" required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <!-- URL -->
            <div class="form-group">
                <label for="static-page-url">URL</label>
                <input type="text"
                       class="form-control"
                       id="static-page-url"
                       name="slug"
                       value="{{ $page->slug }}" readonly>
            </div>

            <!-- CONTENT -->
            <div class="form-group pb-3">
                <label for="static-page-content">Контент</label>
                <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}"
                          name="content"
                          id="static-page-content"
                          rows="20"
                          placeholder="Контент"
                          required>{{ old('content') ?? $page->content }}</textarea>

                @if ($errors->has('content'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                @endif
            </div>

            <input type="hidden" name="id" value="{{ $page->id }}">

            <!-- SUBMIT -->
            <div class="form-group pt-2">
                <button type="submit" class="btn btn-primary">Обновить</button>
            </div>

        </form>

    </div>
@endsection
