@extends('layouts.admin.app')

@section('content')
    <div class="container orders-table-container">
        <br>
        <h3 class="text-center">Бронь</h3>
        <br>

        @include('admin.widgets.success_message')

        <div class="table-responsive table-desktop">
            <table class="table table-bordered text-center orders-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>Номер</th>
                        <th>Даты</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach($orders as $order)
                        <tr class="{{ $order->viewed ? '' : 'violet-background-light' }}">
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->phone }}</td>
                            <td>номер</td>
                            <td class="text-nowrap">{{ $order->from->format('d-m-Y') }}<br>{{ $order->to->format('d-m-Y')}}
                            </td>
                            <td>
                                <a href="{{ route('orders.show', $order->id) }}" class="btn btn-outline-dark btn-sm">Подробнее</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive table-mobile">
            <table class="table table-bordered text-center orders-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Данные</th>
                    <th>Номер</th>
                    <th>Даты</th>
                </tr>
                </thead>
                <tbody class="text-center">
                @foreach($orders as $order)
                    <tr>
                        <td>
                            <a href="{{ route('orders.show', $order->id) }}" class="btn btn-outline-dark btn-sm">{{ $order->id }}</a>
                        </td>
                        <td>
                            {{ $order->name }}
                            <br>
                            {{ $order->phone }}
                        </td>
                        <td>номер</td>
                        <td class="text-nowrap">{{ $order->from->format('d-m-Y') }}<br>{{ $order->to->format('d-m-Y')}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        {{ $orders->links() }}
    </div>
@endsection
