@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <br>
        <h3 class="text-center mb-4">Бронь ID: №{{ $order->id }}</h3>
        <hr>

        <div class="row mt-4">
            <div class="col-lg-6">
                <p><strong>Имя:&nbsp;</strong>{{ $order->name }}</p>
                <p><strong>Тел:&nbsp;</strong>{{ $order->phone }}</p>
                <p><strong>Взрослых:&nbsp;</strong>{{ $order->adult }}</p>
                <p><strong>Детей:&nbsp;</strong>{{ $order->children }}</p>
                <p><strong>Номер:&nbsp;</strong>{{ $order->number_title }}</p>
                <p><strong>Даты:&nbsp;</strong>{{ $order->from->format('d.m.Y') }} - {{ $order->to->format('d.m.Y') }}</p>
                <p><strong>Сумма:&nbsp;</strong>{{ $order->sum }} грн</p>
            </div>
            <div class="col-lg-6">
                <p><strong>Комментарий:</strong>
                    @if($order->comment)
                        {{ $order->comment }}
                    @else
                        <i>без комментариев</i>
                    @endif
                </p>
                <p><strong>Дата создания:</strong>&nbsp;{{ $order->created_at }}</p>
                <form method="POST" action="{{ route('orders.destroy', $order->id) }}">
                    @csrf
                    @method('DELETE')

                    <div class="form-group">
                        <input type="submit" class="btn btn-outline-danger btn-sm delete-order " value="Удалить">
                    </div>
                </form>
            </div>
        </div>
        <hr>
    </div>
@endsection
