<!-- Cropper modal -->
<div class="modal" id="{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="number-cropper-modal" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-outline-primary {{ $btnClass }}">Сохранить изменения</button>

                <div class="loading">
                    <img src="{{ asset('storage/img/loading.gif') }}" alt="loading">
                    <span>Загружаю...</span>
                </div>

                <div class="error-load-side ml-3">
                    <span class="text-danger">Произошла ошибка при загрузке фото</span>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center p-0 mb-5">
                <img src="" alt="image-cropper" class="rounded cropper-image">
            </div>
        </div>
    </div>
</div>
