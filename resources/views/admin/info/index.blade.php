@extends('layouts.admin.app')

@section('content')
    <br>
    <h3 class="text-center">Инфо</h3>
    <br>

    <div class="container">

        @include('admin.widgets.success_message')

        <a href="{{ route('info.create') }}" class="btn btn-outline-primary">Создать</a>

        <br>
        <br>

        <table class="table table-hover">
            <thead>
                <tr class="text-center">
                    <th scope="col">Название</th>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($info as $item)
                    <tr class="text-center">
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->key }}</td>
                        <td>{{ $item->value }}</td>
                        <td class="text-primary">
                            <a href="{{ route('info.edit', ['id' => $item->id]) }}">
                                <i class="fas fa-edit fa-lg"></i>
                            </a>
                        </td>
                        <td class="text-danger">
                            <form action="{{ route('info.destroy', [$item->id]) }}" method="POST" class="info-remove-form">
                                @csrf
                                @method('DELETE')
                                <i class="far fa-trash-alt fa-lg info-remove"></i>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection