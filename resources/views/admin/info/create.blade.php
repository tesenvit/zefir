@extends('layouts.admin.app')

@section('content')
    <br>
    <h3 class="text-center">Создание инфо значения</h3>
    <br>

    <div class="container">

        @include('admin.widgets.success_message')

        <form action="{{ route('info.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="form-group">
                <label for="info-title">Название</label>
                <input type="text"
                       class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                       id="info-title"
                       name="title"
                       required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="info-key">Ключ</label>
                <input type="text"
                       class="form-control {{ $errors->has('key') ? ' is-invalid' : '' }}"
                       id="info-key"
                       name="key"
                       required>

                @if ($errors->has('key'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('key') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="info-value">Значение</label>
                <input type="text"
                       class="form-control {{ $errors->has('value') ? ' is-invalid' : '' }}"
                       id="info-value"
                       name="value"
                       required>

                @if ($errors->has('value'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('value') }}</strong>
                    </span>
                @endif
            </div>

            <br>

            <button type="submit" class="btn btn-outline-primary">Сохранить</button>

        </form>
    </div>
@endsection