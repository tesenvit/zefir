<table width="100% " border="0" style="border-spacing: 0 10px;">
    <caption style="text-align: left; padding-left: 20px; background-color: #2A7F7E">
        <p style="font-weight: bold;font-size: 24px;color: white">
            Бронь <a href="{{ route('orders.show', $order->id) }}" style="color: white">#{{ $order->id }}</a>
        </p>
    </caption>
    <tbody>
        <tr>
            <td style="padding-left: 20px;">Имя: <b>{{ $order->name }}</b></td>
        </tr>
        <tr>
            <td style="padding-left: 20px">Телефон: <b>{{ $order->phone }}</b></td>
        </tr>
        <tr>
            <td style="padding-left: 20px">Врозлых: <b>{{ $order->adult }}</b></td>
        </tr>
        <tr>
            <td style="padding-left: 20px">Детей: <b>{{ $order->children }}</b></td>
        </tr>
        <tr>
            <td style="padding-left: 20px">Даты: <b>{{ $order->from->format('d.m.Y') }} - {{ $order->to->format('d.m.Y') }}</b></td>
        </tr>
        <tr>
            <td style="padding-left: 20px">Номер: <b>{{ $order->number_title }}</b></td>
        </tr>
        <tr>
            <td style="padding-left: 20px">Сумма: <b>{{ $order->sum }} грн.</b></td>
        </tr>
        <tr>
            <td style="padding-left: 20px">Комментарий: <b>{{ $order->comment }}</b></td>
        </tr>
        <tr>
            <td style="padding: 10px 20px; background-color: #2b2e34;">
                <a href="https://zefir-zatoka.com.ua/reserve" style="color: white">
                    https://zefir-zatoka.com.ua
                </a>
            </td>
        </tr>
    </tbody>
</table>
