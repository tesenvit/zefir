@extends('layouts.front.app')

@section('content')
    <blockquote class="blockquote text-center mt-4">
        <h1 class="h2 line-in-the-middle text-center color-dark-grey main-title">
            <i class="far fa-image section-name-icon"></i>&nbsp;
            Галерея
        </h1>
    </blockquote>

    <div class="container">
        @if(!empty($gallery))
            <h4 class="mb-4">Общие</h4>

            <div class="row">
                @foreach($gallery as $image)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-6">
                        <a data-fancybox="gallery" href="{{ asset('storage/gallery/' . $image->path) }}">
                            <img class="img-thumbnail mb-4 gallery-image" src="{{ asset('storage/gallery/thumbnails/' . $image->thumbnail) }}" alt="gallery-image">
                        </a>
                    </div>
                @endforeach
            </div>
        @endif

        @if(settings('gallery_enable_numbers_photo'))
            <hr>
            @foreach($numbers as $number)
                @if($number->images->isNotEmpty())
                    <h4 class="my-4">{{ $number->title }}</h4>

                    <div class="row">
                        @foreach($number->images->sortBy('sort') as $image)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-6">
                                <a data-fancybox="gallery" href="{{ asset('storage/numbers/' . $image->path) }}">
                                    <img class="img-thumbnail mb-4 gallery-image" src="{{ asset('storage/numbers/thumbnails/' . $image->thumbnail) }}" alt="gallery-image">
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            @endforeach
        @endif
    </div>
@endsection
