{{--<blockquote class="blockquote text-center">--}}
{{--    <h1 class="line-in-the-middle h2 color-dark-grey main-title">--}}
{{--        <i class="fas fa-home section-name-icon"></i>&nbsp;--}}
{{--        Мини-отель &laquo;Zefir&raquo;--}}
{{--    </h1>--}}

{{--    {!! text('mainPageOverSlider') !!}--}}

{{--</blockquote>--}}

<div class="container-fluid p-0 main-slider-desktop">
    <div id="main-slider" class="owl-carousel owl-theme">
        @foreach($slidesDesktop as $slide)
            <div class="item">
                <img src="{{ asset('storage/slider/' . $slide->path) }}" class="main-slide-img" alt="slide">
            </div>
        @endforeach
    </div>
</div>

<div class="container-fluid p-0 main-slider-mobile">
    <div id="main-slider-mobile" class="owl-carousel owl-theme">
        @foreach($slidesMobile as $slide)
            <div class="item">
                <img src="{{ asset('storage/slider/' . $slide->path) }}" alt="slide">
            </div>
        @endforeach
    </div>
</div>

<div class="m-b-40"></div>
