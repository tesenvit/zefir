<blockquote class="blockquote text-center">
    <h2 class="line-in-the-middle text-center color-dark-grey main-title">
        <i class="fas fa-key section-name-icon"></i>&nbsp;
        Забронировать
    </h2>

    {!! text('mainPageOverBooking') !!}

</blockquote>

<div class="container pb-5">
    <a name="bookingform"></a>
    <form action="{{ route('order.reserve') }}" method="POST">
        @csrf
        @method('POST')

        <div class="form-row">
            <div class="form-group col-lg-6">
                <label class="sr-only" for="name">Имя</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text {{ $errors->has('name') ? 'is-invalid-custom-badge' : '' }}">
                            <i class="fas fa-pencil-alt booking-from-badge-name"></i>
                        </div>
                    </div>

                    <input type="text"
                           class="form-control {{ $errors->has('name') ? ' is-invalid is-invalid-custom-input' : '' }}"
                           id="name"
                           name="name"
                           placeholder="Имя"
                           value="{{ old('name') }}"
                           required>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif

                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="sr-only" for="phone">Телефон</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text {{ $errors->has('phone') ? 'is-invalid-custom-badge' : '' }}">
                            <i class="fas fa-phone booking-from-badge-phone"></i>
                        </div>
                    </div>

                    <input type="text"
                           class="form-control {{ $errors->has('phone') ? ' is-invalid is-invalid-custom-input' : '' }}"
                           id="phone"
                           name="phone"
                           placeholder="Телефон"
                           value="{{ old('phone') }}"
                           required>

                    @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif

                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-lg-6">
                <label class="sr-only" for="adult">Количество взрослых</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text {{ $errors->has('adult') ? 'is-invalid-custom-badge' : '' }}">
                            <i class="fas fa-user-friends booking-from-badge-adult"></i>
                        </div>
                    </div>

                    <input type="text"
                           class="form-control {{ $errors->has('adult') ? ' is-invalid is-invalid-custom-input' : '' }}"
                           id="adult"
                           name="adult"
                           placeholder="Количество взрослых"
                           value="{{ old('adult') }}"
                           required>

                    @if ($errors->has('adult'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('adult') }}</strong>
                        </span>
                    @endif

                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="sr-only" for="children">Количество детей</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text {{ $errors->has('children') ? 'is-invalid-custom-badge' : '' }}">
                            <i class="fas fa-baby-carriage booking-from-badge-children"></i>
                        </div>
                    </div>

                    <input type="text"
                           class="form-control {{ $errors->has('children') ? ' is-invalid is-invalid-custom-input' : '' }}"
                           id="children"
                           name="children"
                           placeholder="Количество детей"
                           value="{{ old('children') }}"
                           required>

                    @if ($errors->has('children'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('children') }}</strong>
                        </span>
                    @endif

                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-lg-6">
                <label class="sr-only" for="numberType">Номер</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text {{ $errors->has('number_type') ? 'is-invalid-custom-badge' : '' }}">
                            <i class="fas fa-bed booking-from-badge-number"></i>
                        </div>
                    </div>

{{--                    <div id="tager_inputs" class="clearfix">--}}

{{--                    </div>--}}

                    <select id="numberType"
                            class="form-control tager number-type {{ $errors->has('number_type') ? ' is-invalid is-invalid-custom-input' : '' }}"
                            name="number_type"
                            required>

                        @foreach($numbers as $number)
                            <option {{ old('number_type') == $number->id ? 'selected' : '' }}
                                    value="{{ $number->id }}">
                                {{ $number->title }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('number_type'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('number_type') }}</strong>
                        </span>
                    @endif

                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="sr-only" for="date">Даты проживания</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text {{ $errors->has('date') ? 'is-invalid-custom-badge' : '' }}">
                            <i class="far fa-calendar-alt booking-from-badge-date"></i>
                        </div>
                    </div>

                    <input type="text"
                           class="form-control {{ $errors->has('date') ? ' is-invalid is-invalid-custom-input' : '' }}"
                           id="date"
                           name="date"
                           placeholder="Даты проживания"
                           value="{{ old('date') }}"
                           required>

                    @if ($errors->has('date'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif

                </div>
            </div>
        </div>

        <div class="form-group col-lg-12 p-0">
            <textarea class="form-control {{ $errors->has('comment') ? ' is-invalid ' : '' }}"
                      id="comment"
                      rows="3"
                      name="comment"
                      placeholder="Комментарий">{{ old('comment') }}</textarea>

            @if ($errors->has('comment'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('comment') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-row p-1">
            <div class="form-group col-lg-12">
                <div class="form-check {{ $errors->has('terms') ? ' is-invalid is-invalid-custom-checkbox' : '' }}">

                    <input class="form-check-input"
                           type="checkbox"
                           id="terms"
                           name="terms"
                           {{ old('terms') ? ' checked' : '' }}
                           required>

                    <label class="form-check-label" for="terms">
                        Я соглашаюсь с <a class="font-italic" data-toggle="modal" href="#" data-target="#page-rules">условиями проживания</a>
                    </label>
                </div>

                @if ($errors->has('terms'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('terms') }}</strong>
                    </span>
                @endif

            </div>
        </div>

        <input type="hidden" name="typeForm" value="mainPage">
        <div class="form-row p-1">
            <div class="form-group col-lg-12">
                <button type="submit" class="btn btn-outline-dark">Забронировать</button>
            </div>
        </div>
    </form>
</div>
