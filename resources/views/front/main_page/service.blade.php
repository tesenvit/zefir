<blockquote class="blockquote text-center">
    <h2 class="line-in-the-middle text-center color-dark-grey main-title">
        <i class="fas fa-concierge-bell section-name-icon"></i>&nbsp;
        Мы предлагаем
    </h2>

    {!! text('mainPageOverService') !!}

</blockquote>

<div class="container">
    <div class="row text-center service-icons-block">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 service-icon-block">
            <div class="service-icon-img-box pb-4">
                <img class="service-icon-img" src="{{ 'storage/img/hotel.svg' }}" alt="hotel">
            </div>
            <div class="color-dark-grey service-icon-text">
                <span>Комфортабельные</span>
                <br>
                <span>номера</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 service-icon-block">
            <div class="service-icon-img-box pb-4">
                <img class="service-icon-img" src="{{ 'storage/img/wifi.svg' }}" alt="wifi">
            </div>
            <div class="color-dark-grey service-icon-text">
                <span>Бесплатный</span>
                <br>
                <span>Wi-Fi</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 service-icon-block">
            <div class="service-icon-img-box pb-4">
                <img class="service-icon-img" src="{{ 'storage/img/carousel.svg' }}" alt="carousel">
            </div>
            <div class="color-dark-grey service-icon-text">
                <span>Детская</span>
                <br>
                <span>площадка</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 service-icon-block">
            <div class="service-icon-img-box pb-4">
                <img class="service-icon-img" src="{{ 'storage/img/sun.svg' }}" alt="sun">
            </div>
            <div class="color-dark-grey service-icon-text">
                <span>10 минут ходьбы</span>
                <br>
                <span>до моря</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 service-icon-block">
            <div class="service-icon-img-box pb-4">
                <img class="service-icon-img" src="{{ 'storage/img/cutlery.svg' }}" alt="cutlery">
            </div>
            <div class="color-dark-grey service-icon-text">
                <span>Кухня для</span>
                <br>
                <span>приготовления</span>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 service-icon-block">
            <div class="service-icon-img-box pb-4">
                <img class="service-icon-img" src="{{ 'storage/img/parking.svg' }}" alt="parking">
            </div>
            <div class="color-dark-grey service-icon-text">
                <span>Бесплатная</span>
                <br>
                <span>парковка</span>
            </div>
        </div>
    </div>

</div>

{{--<div class="m-b-80"></div>--}}