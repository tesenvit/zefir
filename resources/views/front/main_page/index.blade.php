@extends('layouts.front.app')

@section('content')

    @include('front.main_page.slider')

    @include('front.main_page.numbers')

    @include('front.main_page.service')

    @include('front.main_page.booking_form')

@endsection