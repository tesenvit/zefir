<blockquote class="blockquote text-center">
    <h2 class="line-in-the-middle text-center color-dark-grey main-title">
        <i class="fas fa-bed section-name-icon"></i>&nbsp;
        Наши номера
    </h2>

    {!! text('mainPageOverNumbers') !!}

</blockquote>

<div class="container">
    <div class="row">
        <div class="owl-carousel owl-theme" id="numbers-slider">
            @foreach($numbers as $number)
                <div class="item">
                    <div class="col-12 main-page-card-number">
                        <div class="card main-page-card-number-body">
                            <div class="main-page-card-number-image">
                                <img class="card-img-top" src="{{ asset('storage/numbers/' . $number->getMainImagePath('thumbnail'))  }}" alt="number image">
                                <div class="main-page-card-number-image-layer">
                                    <p class="main-page-card-number-image-layer-text">{{ $number->getPrice() ? $number->getPrice() . ' грн' :'Цена не указана'}}</p>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text main-page-card-number-title color-dark-grey">
                                    <strong>{{ $number->title }}</strong>
                                </p>
                                <p>{{ $number->preview }}</p>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left numbers-btn">
                                        <a href="{{ route('number.show', [$number->slug]) }}" class="btn btn-sm btn-outline-dark">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="m-b-40"></div>
