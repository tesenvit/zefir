@extends('layouts.front.app')

@section('content')
    <blockquote class="blockquote text-center mt-4">
        <h1 class="h2 line-in-the-middle text-center color-dark-grey main-title">
            <i class="fas fa-bed section-name-icon"></i>&nbsp;
            {{ $number->title }}
        </h1>
    </blockquote>

    <div class="container container-with-sliders">
        <div id="slider-big" class="owl-carousel owl-theme">
            @foreach($number->images->sortBy('sort') as $image)
                <div class="item">
                    <img class="card-img-top rounded" src="{{ asset('storage/numbers/' . $image->path)  }}" alt="number image">
                </div>
            @endforeach
        </div>

        <div id="slider-thumbs" class="owl-carousel owl-theme">
            @foreach($number->images->sortBy('sort') as $image)
                <div class="item">
                    <img class="card-img-top rounded" src="{{ asset('storage/numbers/thumbnails/' . $image->thumbnail)  }}" alt="number image">
                </div>
            @endforeach
        </div>
    </div>

    <div class="container">

        <div class="m-b-40"></div>
        <hr>
        <div class="m-b-40"></div>

        <div class="number-description">
            <p>{!! $number->description !!}</p>
        </div>

        <div class="m-b-40"></div>
        <hr>
        <div class="m-b-40"></div>

        <a name="bookingform"></a>
        <form class="number-form-booking" method="POST" action="{{ route('order.reserve') }}">

            @csrf
            @method('POST')

            <div class="form-row">
                <div class="form-group col-lg-4 offset-lg-2 col-md-5 offset-md-1">
                    <label class="sr-only" for="name">Имя</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text {{ $errors->has('name') ? 'is-invalid-custom-badge' : '' }}">
                                <i class="fas fa-pencil-alt booking-from-badge-name"></i>
                            </div>
                        </div>

                        <input type="text"
                               class="form-control {{ $errors->has('name') ? ' is-invalid is-invalid-custom-input' : '' }}"
                               id="name"
                               name="name"
                               placeholder="Имя"
                               value="{{ old('name') }}"
                               required>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="form-group col-lg-4 col-md-5">
                    <label class="sr-only" for="phone">Телефон</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text {{ $errors->has('phone') ? 'is-invalid-custom-badge' : '' }}">
                                <i class="fas fa-phone booking-from-badge-phone"></i>
                            </div>
                        </div>

                        <input type="text"
                               class="form-control {{ $errors->has('phone') ? ' is-invalid is-invalid-custom-input' : '' }}"
                               id="phone"
                               name="phone"
                               placeholder="Телефон"
                               value="{{ old('phone') }}"
                               required>

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-lg-4 offset-lg-2 col-md-5 offset-md-1">
                    <label class="sr-only" for="adult">Количество взрослых</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text {{ $errors->has('adult') ? ' is-invalid is-invalid-custom-input' : '' }}">
                                <i class="fas fa-user-friends booking-from-badge-adult"></i>
                            </div>
                        </div>

                        <input type="text"
                               class="form-control"
                               id="adult"
                               name="adult"
                               placeholder="Количество взрослых"
                               value="{{ old('adult') }}"
                               required>

                        @if ($errors->has('adult'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('adult') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="form-group col-lg-4 col-md-5">
                    <label class="sr-only" for="children">Количество детей</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text {{ $errors->has('children') ? 'is-invalid-custom-badge' : '' }}">
                                <i class="fas fa-baby-carriage booking-from-badge-children"></i>
                            </div>
                        </div>

                        <input type="text"
                               class="form-control"
                               id="children"
                               name="children"
                               placeholder="Количество детей"
                               value="{{ old('children') }}"
                               required>

                        @if ($errors->has('children'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('children') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-lg-4 col-md-5 offset-lg-2 offset-md-1">
                    <label class="sr-only" for="date">Даты проживания</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text {{ $errors->has('date') ? 'is-invalid-custom-badge' : '' }}">
                                <i class="far fa-calendar-alt booking-from-badge-date"></i>
                            </div>
                        </div>

                        <input type="text"
                               class="form-control {{ $errors->has('date') ? ' is-invalid is-invalid-custom-input' : '' }}"
                               id="date"
                               name="date"
                               placeholder="Даты проживания"
                               value="{{ old('date') }}"
                               required>

                        @if ($errors->has('date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>
            </div>

            <div class="form-group col-lg-8 offset-lg-2 col-md-10 offset-md-1 p-0">
                <textarea class="form-control {{ $errors->has('comment') ? ' is-invalid ' : '' }}"
                          id="comment"
                          rows="3"
                          name="comment"
                          placeholder="Комментарий">{{ old('comment') }}</textarea>

                @if ($errors->has('comment'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('comment') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-row p-1">
                <div class="form-group col-lg-6 offset-lg-2 col-md-6 offset-md-1">
                    <div class="form-check {{ $errors->has('terms') ? ' is-invalid is-invalid-custom-checkbox' : '' }}">

                        <input class="form-check-input"
                               type="checkbox"
                               id="terms"
                               name="terms"
                               {{ old('terms') ? ' checked' : '' }}
                               required>

                        <label class="form-check-label" for="terms">
                            Я соглашаюсь с <a class="font-italic" data-toggle="modal" href="#" data-target="#page-rules">условиями проживания</a>
                        </label>
                    </div>

                    @if ($errors->has('terms'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('terms') }}</strong>
                        </span>
                    @endif

                </div>
            </div>

            <input type="hidden" name="typeForm" value="number">

            <input type="hidden" name="number_type" value="{{ $number->id }}">

            <div class="form-row p-1">
                <div class="form-group col-lg-4 offset-lg-2 col-md-5 offset-md-1">
                    <button type="submit" class="btn btn-outline-dark">Забронировать</button>
                </div>
            </div>

        </form>
    </div>
@endsection
