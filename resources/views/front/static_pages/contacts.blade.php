@extends('layouts.front.app')

@section('content')

    <blockquote class="blockquote text-center mt-4">
        <h1 class="line-in-the-middle text-center color-dark-grey main-title">
            <i class="far fa-envelope section-name-icon"></i>
            {{ $contactsPage->title }}
        </h1>
    </blockquote>

    <div class="container">

        {!! $contactsPage->content !!}

    </div>

    <div class="container my-4">
        <hr>
    </div>

    <div class="container">
        <form action="{{ route('feedback.store') }}" method="POST" class="pb-4">
            @csrf
            @method('POST')

            <div class="form-row">
                <div class="form-group col-lg-4 offset-lg-2 col-md-5 offset-md-1">
                    <h4>Напишите нам</h4>
                </div>
            </div>

            <div class="form-row">

                <!-- NAME -->
                <div class="form-group col-lg-4 offset-lg-2 col-md-5 offset-md-1">
                    <label class="sr-only" for="feedback-name">Имя</label>
                    <div class="input-group mb-2">

                        <div class="input-group-prepend">
                            <div class="input-group-text {{ $errors->has('name') ? 'is-invalid-custom-badge' : '' }}">
                                <i class="fas fa-pencil-alt booking-from-badge-name"></i>
                            </div>
                        </div>

                        <input type="text"
                               class="form-control{{ $errors->has('name') ? ' is-invalid is-invalid-custom-input' : '' }}"
                               id="feedback-name"
                               name="name"
                               placeholder="Имя"
                               value="{{ old('name') }}"
                               required>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- EMAIL -->
                <div class="form-group col-lg-4 col-md-5">
                    <label class="sr-only" for="feedback-email">Email</label>
                    <div class="input-group mb-2">

                        <div class="input-group-prepend">
                            <div class="input-group-text {{ $errors->has('email') ? 'is-invalid-custom-badge' : '' }}">
                                <i class="far fa-envelope feedback-envelope-badge"></i>
                            </div>
                        </div>

                        <input type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid is-invalid-custom-input' : '' }}"
                               id="feedback-email"
                               name="email"
                               placeholder="Email"
                               value="{{ old('email') }}"
                               required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>
            </div>

            <!-- TEXT -->
            <div class="form-group col-lg-8 offset-lg-2 col-md-10 offset-md-1 px-0">
                <textarea class="form-control {{ $errors->has('text') ? ' is-invalid' : '' }}"
                          id="feedback-text"
                          rows="5"
                          name="text"
                          placeholder="Сообщение"
                          required>{{ old('text') }}</textarea>

                @if ($errors->has('text'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('text') }}</strong>
                    </span>
                @endif
            </div>

            <!-- RECAPTCHA -->
            <div class="form-group col-lg-8 offset-lg-2 col-md-10 offset-md-1 px-0">

                <script src='https://www.google.com/recaptcha/api.js'></script>

                <div class="g-recaptcha {{ $errors->has('g-recaptcha-response') ? ' is-invalid-recaptcha' : '' }}" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">

                </div>

                @if ($errors->has('g-recaptcha-response'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                    </span>
                @endif

            </div>

            <!-- SUBMIT -->
            <div class="form-group col-lg-8 offset-lg-2 col-md-10 offset-md-1 px-0">
                <button type="submit" class="btn btn-outline-dark">Отправить</button>
            </div>

        </form>

    </div>
@endsection
