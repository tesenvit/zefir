@extends('layouts.front.app')

@section('content')
    <blockquote class="blockquote text-center mt-4">
        <h1 class="h2 line-in-the-middle text-center color-dark-grey main-title">
            <i class="fas fa-list-ul section-name-icon"></i>
            {{ $aboutPage->title }}
        </h1>
    </blockquote>


    <div class="container">
        {!! $aboutPage->content !!}
    </div>

    <div class="container my-4">
        <hr>
    </div>

    <div class="container pb-4">
        <iframe class="google-map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1382.5276072154272!2d30.5120242011591!3d46.12973002253775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x390ee09a2bdefb42!2z0JfQtdGE0LjRgA!5e0!3m2!1sru!2sua!4v1561730482855!5m2!1sru!2sua"
                frameborder="0"
                allowfullscreen>
        </iframe>
    </div>

@endsection
