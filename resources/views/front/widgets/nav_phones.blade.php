<div class="modal fade" id="nav-phones" tabindex="-1" role="dialog" aria-labelledby="nav-phones-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="nav-phones-modal-title">
                    <i class="fas fa-phone mr-3"></i>
                    <span>Наши телефоны</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                @foreach(information('phones') as $phone)
                    <p class="modal-nav-phone">
                        <a href="tel:{{ $phone }}">{{ $phone }}</a>
                    </p>
                @endforeach
            </div>
        </div>
    </div>
</div>