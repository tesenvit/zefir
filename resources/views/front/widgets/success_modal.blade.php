<!-- Success modal -->
<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="success-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">

            <div class="box-icon-check">
                <i class="fas fa-check"></i>
            </div>

            <div class="modal-body text-center pt-4">
                <h3>Успех!</h3>

                <p class="py-3">{{ session('successModal') }}</p>

                <div class="pb-2">
                    <button type="button" class="btn btn-outline-dark btn-sm w-75" data-dismiss="modal">Закрыть</button>
                </div>
            </div>

        </div>
    </div>
</div>
