/*
|--------------------------------------------------------------------------
| MAIN START
|--------------------------------------------------------------------------
*/
$(function () {
    /* Close */
    $('#slider-cropper-modal .close, #number-cropper-modal .close, #gallery-cropper-modal .close').on('click', function () {
        $('#slider-photo-file').val('');
        $('#number-photo-file').val('');
        $('#gallery-photo-file').val('');
        $('.btn-cropper-modal').css('display', 'none');
    });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/* Navigation dropdowns */
if($(window).width() > 767)
{
    $(".navigation-dropdown-menu").css('margin-top',0);
    $(".navigation-dropdown")
        .mouseover(function() {
            $(this).addClass('show').attr('aria-expanded',"true");
            $(this).find('.dropdown-menu').addClass('show');
        })
        .mouseout(function() {
            $(this).removeClass('show').attr('aria-expanded',"false");
            $(this).find('.dropdown-menu').removeClass('show');
        });
}
/*
|--------------------------------------------------------------------------
| MAIN END
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| SLIDERS START
|--------------------------------------------------------------------------
*/
$(function() {
    /* Cropper */
    $("#slider-photo-file").change(function () {

        let file = $(this).prop('files')[0];
        let reader = new FileReader();
        var ratio = (getSliderType() === "desktop") ? 15 / 7 : 4 / 3;

        reader.readAsDataURL(file);

        reader.onload = function (e) {

            let img = $('.cropper-image');

            img.cropper('destroy');
            img.attr('src', e.target.result);
            img.cropper({
                aspectRatio: ratio,
                viewMode: 2,
            });

            $('#slider-cropper-modal').modal('show');
            $('.btn-cropper-modal').css('display', 'inline-block');

        };
    });


    $('.crop-photo-for-slider').on('click', function () {

        $('.loading').css('display', 'block');

        $('.cropper-image').cropper('getCroppedCanvas').toBlob(function (blob) {

            let data = new FormData();

            data.append('image', blob);
            data.append('type', getSliderType());

            $.ajax({
                url: '/admin/slider-save-image',
                data: data,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (slides) {
                    $('.list-slides').html(slides);
                    $('#slider-photo-file').val('');
                    $('.loading').css('display', 'none');
                    $('.btn-cropper-modal').css('display', 'none');
                    $('.success-load-photo').css('display', 'block');
                    $('#slider-cropper-modal').modal('hide');

                    setTimeout(function() {
                        $('.success-load-photo').css('display', 'none');
                    }, 3000);
                },
                error: function () {
                    $('.loading').css('display', 'none');
                    $('.error-load-side').css('display', 'block');
                }
            });

        }, 'image/jpeg');

    });

});


/* Sortable */
$(window).on('load', function () {
    $(".sortable-desktop, .sortable-mobile").sortable({
        update: function (event, ui) {

            let ids = [];

            let type = getSliderType();
            let sortable = (type === 'desktop') ? '.sortable-desktop .card' : '.sortable-mobile .card';

            $(sortable).each(function (i,v) {
                ids.push($(v).data('id'));
            });

            $.ajax({
                data: {
                    ids: ids,
                },
                type: 'POST',
                url: '/admin/slider-sort-images',
                success: function (data) {

                }
            });
        }
    });
});

/* Remove */
$(document).on('click', '.slide-remove', function () {
    let confirm = window.confirm('Вы уверены, что хотите УДАЛИТЬ фото?');

    if(confirm)
    {
        let id = $(this).parents('.card').data('id');

        let type = getSliderType();

        $.ajax({
            data: {
                id: id,
                type: type
            },
            type: 'POST',
            url: '/admin/slider-remove-image',
            success: function (slides) {
                $('.list-slides').html(slides);
                $('.success-remove-photo').css('display', 'block');

                setTimeout(function() {
                    $('.success-remove-photo').css('display', 'none');
                }, 3000);
            }
        });
    }

});

/* Switch active */
$(document).on('click', '.slide-switch-active', function () {
    let id = $(this).parents('.card').data('id');
    let type = getSliderType();

    $.ajax({
        data: {
            id: id,
            type: type
        },
        type: 'POST',
        url: '/admin/slider-switch-active',
        success: function (slides) {
            $('.list-slides').html(slides);
        }
    });
});
/*
|--------------------------------------------------------------------------
| SLIDERS END
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| NUMBER START
|--------------------------------------------------------------------------
*/
$(function() {

    /* Cropper */
    $("#number-photo-file").change(function () {

        let file = $(this).prop('files')[0];
        let reader = new FileReader();

        reader.readAsDataURL(file);

        reader.onload = function (e) {

            let img = $('.cropper-image');

            img.cropper('destroy');
            img.attr('src', e.target.result);
            img.cropper({
                aspectRatio: 16 / 9,
                viewMode: 2,
            });

            $('#number-cropper-modal').modal('show');
            $('.btn-cropper-modal').css('display', 'inline-block');

        };
    });


    $('.crop-image-for-number').on('click', function () {

        $('.loading').css('display', 'block');

        $('.cropper-image').cropper('getCroppedCanvas').toBlob(function (blob) {

            let data = new FormData();
            let id = $('.container-number').data('id');

            data.append('image', blob);
            data.append('id', id);

            $.ajax({
                url: '/admin/number-save-image',
                data: data,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (images) {
                    $('.sortable-number-images').html(images);
                    $('#number-photo-file').val('');
                    $('.loading').css('display', 'none');
                    $('.btn-cropper-modal').css('display', 'none');
                    $('#number-cropper-modal').modal('hide');
                },
                error: function () {
                    $('.loading').css('display', 'none');
                    $('.error-load-side').css('display', 'block');
                }
            });

        }, 'image/jpeg');

    });

    /* Sort */
    $(".sortable-number-images").sortable({
        update: function (event, ui) {

            let ids = [];
            let numberId = $('.container-number').data('id');

            $('.sortable-number-images .card').each(function (i,v) {
                ids.push($(v).data('id'));
            });

            $.ajax({
                data: {
                    ids: ids,
                    numberId: numberId,
                },
                type: 'POST',
                url: '/admin/number-sort-images',
                success: function (data) {

                }
            });
        }
    });

});

/* Switch active image from number*/
$(document).on('click', '.number-image-switch-active', function () {

    let id = $(this).parents('.card').data('id');

    $.ajax({
        data: {
            id: id,
        },
        type: 'POST',
        url: '/admin/number-image-switch-active',
        success: function (images) {
            $('.sortable-number-images').html(images);
        }
    });
});

/* Remove image from number*/
$(document).on('click', '.number-image-remove', function () {
    let confirm = window.confirm('Вы уверены, что хотите УДАЛИТЬ фото?');

    if(confirm)
    {
        let id = $(this).parents('.card').data('id');

        $.ajax({
            data: {
                id: id,
            },
            type: 'POST',
            url: '/admin/number-remove-image',
            success: function (images) {
                $('.sortable-number-images').html(images);
            }
        });
    }

});

/* Added input with price*/
$(document).on('click', '.number-add-price', function () {

    let numberId = $('.container-number').data('id');

    $.ajax({
        data: {
            numberId: numberId,
        },
        type: 'POST',
        url: '/admin/number-add-price',
        success: function (newPrice) {
            $('.number-prices').append(newPrice);
        }
    });
});

/* Remove price for number*/
$(document).on('click', '.number-price-remove', function () {
    let confirm = window.confirm('Вы уверены, что хотите УДАЛИТЬ цену?');

    if(confirm)
    {
        let id = $(this).parents('.number-box-price').data('id');

        $.ajax({
            data: {
                id: id,
            },
            type: 'POST',
            url: '/admin/number-remove-price',
            success: function (id) {
                $('.number-box-price[data-id="'+ id+'"]').remove();
            }
        });
    }
});

/* Datepicker for number prices*/
$(window).on('load', function() {
    $(".number-cost-date").each(function() {

        let dateValue = $(this).val();
        let date = makeDate(dateValue);

        let dp = $(this).datepicker({
            multipleDates: 2,
            multipleDatesSeparator: "-",
            position: "top left",
            range: true,
            dateFormat: 'dd.mm.yyyy'
        }).data('datepicker');

        if(dateValue)
            dp.selectDate([date['from'], date['to']]);

    });
});

/* Switch active of number*/
$(document).on('click', '.number-switch-active', function () {

    let id = $(this).parents('.number-box-data').data('id');

    $.ajax({
        data: {
            id: id,
        },
        type: 'POST',
        url: '/admin/number-switch-active',
        success: function (numbers) {
            $('.list-numbers-body').html(numbers)
        }
    });
});


$(function() {
    /* Sort numbers*/
    $(".list-numbers-body").sortable({
        update: function (event, ui) {

            let ids = [];

            $('.list-numbers-body .number-box-data').each(function (i,v) {
                ids.push($(v).data('id'));
            });

            $.ajax({
                data: {
                    ids: ids,
                },
                type: 'POST',
                url: '/admin/numbers-sort',
                success: function (data) {

                }
            });
        }
    });
});

/* Remove number */
$(document).on('click', '.number-remove' ,function () {

    let confirmOne = window.confirm('Вы уверены, что хотите УДАЛИТЬ номер?');

    if(confirmOne)
    {
        let confirmTwo = window.confirm('Будут удалены ВСЕ фотографии номера');

        if(confirmTwo)
        {
            $('.number-remove-form').submit();
        }

        return false;
    }

    return false;
});

/* CKeditor */
if(document.querySelector('#number-description'))
{
    CKEDITOR.replace('number-description');
}

/*
|--------------------------------------------------------------------------
| NUMBER END
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| STATIC PAGES START
|--------------------------------------------------------------------------
*/
/* CKeditor */
if(document.querySelector('#static-page-content'))
{
    CKEDITOR.replace('static-page-content');
}

/*
|--------------------------------------------------------------------------
| STATIC PAGES END
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| GALLERY START
|--------------------------------------------------------------------------
*/
$(function () {

    /* Cropper */
    $("#gallery-photo-file").change(function () {

        let file = $(this).prop('files')[0];
        let reader = new FileReader();

        reader.readAsDataURL(file);

        reader.onload = function (e) {

            let img = $('.cropper-image');

            img.cropper('destroy');
            img.attr('src', e.target.result);
            img.cropper({
                aspectRatio: 16 / 9,
                viewMode: 2,
            });

            $('#gallery-cropper-modal').modal('show');
            $('.btn-cropper-modal').css('display', 'inline-block');

        };
    });

    $('.crop-photo-for-gallery').on('click', function () {

        $('.loading').css('display', 'block');

        $('.cropper-image').cropper('getCroppedCanvas').toBlob(function (blob) {

            let data = new FormData();

            data.append('image', blob);

            $.ajax({
                url: '/admin/gallery-save-image',
                data: data,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (images) {
                    $('.sortable-gallery-images').html(images);
                    $('#gallery-photo-file').val('');
                    $('.loading').css('display', 'none');
                    $('.btn-cropper-modal').css('display', 'none');
                    $('#gallery-cropper-modal').modal('hide');
                },
                error: function () {
                    $('.loading').css('display', 'none');
                    $('.error-load-side').css('display', 'block');
                }
            });

        }, 'image/jpeg');

    });

    /* Sort */
    $(".sortable-gallery-images").sortable({
        update: function (event, ui) {

            let ids = [];

            $('.sortable-gallery-images .card').each(function (i,v) {
                ids.push($(v).data('id'));
            });

            $.ajax({
                data: {
                    ids: ids
                },
                type: 'POST',
                url: '/admin/gallery-sort-images',
                success: function (data) {

                }
            });
        }
    });

});

/* Switch active image from number*/
$(document).on('click', '.gallery-image-switch-active', function () {

    let id = $(this).parents('.card').data('id');

    $.ajax({
        data: {
            id: id,
        },
        type: 'POST',
        url: '/admin/gallery-image-switch-active',
        success: function (images) {
            $('.sortable-gallery-images').html(images);
        }
    });
});

/* Remove image from number*/
$(document).on('click', '.gallery-image-remove', function () {
    let confirm = window.confirm('Вы уверены, что хотите УДАЛИТЬ фото?');

    if(confirm)
    {
        let id = $(this).parents('.card').data('id');

        $.ajax({
            data: {
                id: id,
            },
            type: 'POST',
            url: '/admin/gallery-remove-image',
            success: function (images) {
                $('.sortable-gallery-images').html(images);
            }
        });
    }

});

/* Enable show photos of numbers */
$(document).on('click', '.gallery-enable-numbers-photo', function () {
    $.ajax({
        type: 'POST',
        url: '/admin/gallery-enable-numbers-photo',
        success: function (button) {
            $('.gallery-enable-numbers-photo-box').html(button);
        }
    });
});
/*
|--------------------------------------------------------------------------
| GALLERY END
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| INFO START
|--------------------------------------------------------------------------
*/
$(document).on('click', '.info-remove' ,function () {

    let confirmOne = window.confirm('Вы уверены, что хотите УДАЛИТЬ значение?');

    if(confirmOne)
    {
        $('.info-remove-form').submit();

        return false;
    }

    return false;
});
/*
|--------------------------------------------------------------------------
| INFO END
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| TEXT START
|--------------------------------------------------------------------------
*/
/* Remove */
$(document).on('click', '.text-remove' ,function () {

    let confirmOne = window.confirm('Вы уверены, что хотите УДАЛИТЬ текст?');

    if(confirmOne)
    {
        $('.text-remove-form').submit();

        return false;
    }

    return false;
});

/* CKeditor */
if(document.querySelector('#text-value'))
{
    CKEDITOR.replace('text-value');
}

/*
|--------------------------------------------------------------------------
| TEXT END
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| ORDERS START
|--------------------------------------------------------------------------
*/
$(document).on('click', '.delete-order' ,function (e) {

    let confirmOne = window.confirm('Вы хотите удалить заказ?');

    if(confirmOne)
    {
        $(e.target).closest('form').submit();

        return false;
    }

    return false;
});

/*
|--------------------------------------------------------------------------
| ORDERS END
|--------------------------------------------------------------------------
*/
