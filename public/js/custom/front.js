$( document ).ready(function() {

    /* Booking form datepicker */
    $( function() {
        $('#date, #date-widget').datepicker({
            multipleDates: 2,
            multipleDatesSeparator: "-",
            position: "top left",
            range: true,
            minDate: new Date()
        })
    });

    if($('#date').val())
    {
        let dateInput = $('#date');

        let date = makeDate(dateInput.val());

        let dp = dateInput.datepicker({
            multipleDates: 2,
            multipleDatesSeparator: "-",
            position: "top left",
            range: true,
            dateFormat: 'dd.mm.yyyy'
        }).data('datepicker');

        dp.selectDate([date['from'], date['to']]);
    }

    /* Booking form selectpicker */
    // $(document).on('change', '.tager', function () {
    //     $('#tager_inputs').append(
    //         '<div class="tag">' + $(this).find('option:selected').text() + '<span class="delete">X</span><input type="hidden" name="tags[]" value="' + $(this).val() + '"></div>'
    //     );
    // });
    //
    // $(document).on('click', '.tag .delete', function () {
    //     $(this).parents('.tag').remove();
    // });

    /* Main Slider */
    $("#main-slider, #main-slider-mobile").owlCarousel({
        loop:true,
        nav:true,
        navText: [
            '<i class="fas fa-chevron-left" aria-hidden="true" ></i>',
            '<i class="fas fa-chevron-right" aria-hidden="true"></i>'
        ],
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        dots:false,

        animateOut: 'fadeOut',

        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false,

        items : 1,
        itemsDesktop : false,
        itemsDesktopSmall : false,
        itemsTablet: false,
        itemsMobile : false

    });

    /* Numbers Slider on main page*/
    $(document).ready(function() {
        $('#numbers-slider').owlCarousel({
            loop:true,
            nav:true,
            dots:false,
            items: 3,
            navText: [
                '<i class="fas fa-chevron-left" aria-hidden="true" ></i>',
                '<i class="fas fa-chevron-right" aria-hidden="true"></i>'
            ],
            responsive:{
                0:{
                    items:1,
                },
                576:{
                    items:2,
                },
                991:{
                    items:2,
                },
                1200:{
                    items:3,
                },
                1800:{
                    items:3,
                }
            }
        })
    });


    /* Number Sliders */
    $(document).ready(function() {
        var bigimage = $("#slider-big");
        var thumbs = $("#slider-thumbs");
        var syncedSecondary = true;

        bigimage
            .owlCarousel({
                items: 1,
                slideSpeed: 2000,
                nav: true,
                dots: false,
                // autoplay: true,
                loop: true,
                responsiveRefreshRate: 200,
                navText: [
                    '<i class="fas fa-chevron-left" aria-hidden="true"></i>',
                    '<i class="fas fa-chevron-right" aria-hidden="true"></i>'
                ]
            })
            .on("changed.owl.carousel", syncPosition);

        thumbs
            .on("initialized.owl.carousel", function() {
                thumbs
                    .find(".owl-item")
                    .eq(0)
                    .addClass("current");
            })
            .owlCarousel({
                items: 10,
                dots: false,
                smartSpeed: 200,
                slideSpeed: 500,
                slideBy: 10,
                responsiveRefreshRate: 100,
                margin: 7,
                responsive:{
                    0:{
                        items:4,
                    },
                    320:{
                        items:4,
                    },
                    450:{
                        items:5,
                    },
                    576:{
                        items:6,
                    },
                    767:{
                        items:7,
                    },
                    991:{
                        items:8,
                    },
                    1000:{
                        items:9,
                    },
                    1200:{
                        items:10,
                    },
                    1800:{
                        items:10,
                    }
                }
            })
            .on("changed.owl.carousel", syncPosition2);

        function syncPosition(el) {
            var count = el.item.count - 1;
            var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

            if (current < 0)
                current = count;

            if (current > count)
                current = 0;

            thumbs
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
            var onscreen = thumbs.find(".owl-item.active").length - 1;
            var start = thumbs
                .find(".owl-item.active")
                .first()
                .index();
            var end = thumbs
                .find(".owl-item.active")
                .last()
                .index();

            if (current > end)
                thumbs.data("owl.carousel").to(current, 100, true);

            if (current < start)
                thumbs.data("owl.carousel").to(current - onscreen, 100, true);
        }

        function syncPosition2(el) {
            if (syncedSecondary) {
                var number = el.item.index;
                bigimage.data("owl.carousel").to(number, 100, true);
            }
        }

        thumbs.on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).index();
            bigimage.data("owl.carousel").to(number, 300, true);
        });
    });

    /* Navigation dropdown */
    if($(window).width() > 767)
    {
        $(".navigation-dropdown-menu").css('margin-top',0);
        $(".navigation-dropdown")
            .mouseover(function() {
                $(this).addClass('show').attr('aria-expanded',"true");
                $(this).find('.dropdown-menu').addClass('show');
            })
            .mouseout(function() {
                $(this).removeClass('show').attr('aria-expanded',"false");
                $(this).find('.dropdown-menu').removeClass('show');
            });
    }

    /* Modals*/
    if(successModal)
    {
        $('#success-modal').modal('show')
    }

    if(bookingFormModal)
    {
        $('#nav-booking').modal('show')
    }

});


