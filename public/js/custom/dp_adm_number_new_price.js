$(document).ready(function() {
    $(".number-cost-date").each(function() {
        $(this).datepicker({
            multipleDates: 2,
            multipleDatesSeparator: "-",
            position: "top left",
            range: true,
            dateFormat: 'dd.mm.yyyy'
        }).data('datepicker');
    });
});