$(".sortable-desktop, .sortable-mobile").sortable({
    update: function (event, ui) {

        let ids = [];

        let type = getSliderType();
        let sortable = (type === 'desktop') ? '.sortable-desktop .card' : '.sortable-mobile .card';

        $(sortable).each(function (i,v) {
            ids.push($(v).data('id'));
        });

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                ids: ids,
            },
            type: 'POST',
            url: '/admin/slider-sort-images',
            success: function (data) {

            }
        });
    }
});