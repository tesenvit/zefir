function getSliderType()
{
    let $elements = $('#sliderTabs').find('.nav-link');
    let type = '';

    $elements.each(function (i,e) {
        if($(e).hasClass('active'))
            type = $(e).data('type');
    });

    return type;
}

function makeDate(dateValue)
{
    let date = dateValue.split('-');
    let makeDate = [];

    makeDate['from'] = new Date(makeFormatDate(date[0]));
    makeDate['to'] = new Date(makeFormatDate((date[1]) ? date[1] : date[0]));

    return makeDate;
}

function makeFormatDate(date)
{
    let dateRaw = date.split('.');
    let day = dateRaw[0];
    let month = dateRaw[1];
    let year = dateRaw[2];

    return month + '/' + day + '/' + year;
}
