<?php

use App\Models\Feedback;
use App\Entities\InfoEntity;
use App\Entities\TextEntity;
use App\Entities\SettingEntity;

if (! function_exists('settings'))
{
    function settings($key)
    {
        if (is_array($key))
        {
            SettingEntity::setValue($key);
            return null;
        }

        return SettingEntity::getValue($key);
    }
}

if (! function_exists('information'))
{
    function information($key)
    {
        return InfoEntity::getInstance()->$key();
    }
}

if (! function_exists('text'))
{
    function text($key)
    {
        return TextEntity::getInstance()->$key();
    }
}

if (! function_exists('feedback_count'))
{
    function feedback_count()
    {
        return Feedback::where('viewed', 0)->count();
    }
}