<?php

namespace App\Components;

use Intervention\Image\Facades\Image;

trait Imageable
{
    /**
     * Make image with library Intervention Image
     *
     * @param $file
     * @param string $path
     * @param int $size
     * @param int $quality
     *
     * @return string
     */
    public static function make($file, string $path, int $size, int $quality = 100) :string
    {
        $name = $file->hashName();

        $image = Image::make($file);

        $image->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $image->save(storage_path($path . $name), $quality);

        return $name;
    }
}