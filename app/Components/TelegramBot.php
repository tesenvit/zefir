<?php

namespace App\Components;

use Illuminate\Support\Facades\Log;

class TelegramBot
{
    /**
     * Make message for order
     *
     * @param $order
     * @return void
     */
    public static function sendOrderNotification($order) :void
    {
        $message  = "<b>-~-~-~-~-~-~-~-~-~-~-~-~-</b>" . PHP_EOL;
        $message .= "<b>НОВАЯ БРОНЬ #{$order->id}:</b>" . PHP_EOL;
        $message .= "Имя: <b>{$order->name}</b>" . PHP_EOL;
        $message .= "Телефон: <b>{$order->phone}</b>" . PHP_EOL;
        $message .= "Врозлых: <b>{$order->adult}</b>" . PHP_EOL;
        $message .= "Детей: <b>{$order->children}</b>" . PHP_EOL;
        $message .= "Даты: <b>{$order->from->format('d.m.Y')} - {$order->to->format('d.m.Y')}</b>" . PHP_EOL;
        $message .= "Номер: <b>{$order->number_title}</b>" . PHP_EOL;
        $message .= "Сумма: <b>{$order->sum}</b> грн." . PHP_EOL;
        $message .= "Комментарий: <b>{$order->comment}</b>" . PHP_EOL;
        $message .= "<b>-~-~-~-~-~-~-~-~-~-~-~-~-</b>";

        self::sendMessage($message);
    }

    /**
     * Send message to telegram
     *
     * @param string $message
     * @return void
     */
    private static function sendMessage(string $message) :void
    {
        $token  = env('TELEGRAM_BOT_TOKEN');
        $chatID = env('TELEGRAM_GROUP_ID');
        $message = urlencode($message);

        // https://api.telegram.org/botXXX:YYYY/getUpdate

        $url = "https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chatID}&text=" . $message . "&parse_mode=html";

        try {
            $curl = curl_init();
            $options = [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true
            ];
            curl_setopt_array($curl, $options);
            curl_exec($curl);
            curl_close($curl);
        } catch (\Exception $e) {
           Log::error($e->getMessage());
        }
    }
}
