<?php
namespace App\Components;

use GuzzleHttp\Client;

class ReCaptcha
{
    /**
     * Check validate ReCaptcha
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return mixed
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        $client =  new Client();

        $response = $client->post('https://www.google.com/recaptcha/api/siteverify',
            ['form_params'=>
                [
                    'secret'=>env('GOOGLE_RECAPTCHA_SECRET'),
                    'response'=>$value
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());

        return $body->success;
    }
}