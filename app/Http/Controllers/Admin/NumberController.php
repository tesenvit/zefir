<?php

namespace App\Http\Controllers\Admin;

use App\Models\Number;
use Illuminate\Http\Request;
use App\Entities\NumberEntity;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NumberStoreRequest;
use App\Http\Requests\Admin\NumberUpdateRequest;

class NumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $numbers = Number::orderBy('sort','asc')->get();

        return view('admin.number.index', compact('numbers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.number.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NumberStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NumberStoreRequest $request)
    {
        NumberEntity::adminStore($request);

        return redirect()->route('numbers.index')->with('success', 'Номер создан успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $number = Number::find($id);

        return view('admin.number.edit', compact('number'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NumberUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NumberUpdateRequest $request, $id)
    {
        NumberEntity::adminUpdate($request, $id);

        return redirect()->back()->with('success', 'Данные успешно обновлены!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NumberEntity::adminDestroy($id);

        return redirect()->route('numbers.index')->with('success', 'Номер был успешно УДАЛЕН!');
    }

    /**
     *  Make image with thumbnail and save them
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function saveImage(Request $request)
    {
        $number = NumberEntity::adminSaveImage($request);

        return view('admin.number.list_images', compact('number'))->render();
    }

    /**
     * Sort images with drag and drop
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function sortImages(Request $request)
    {
        $number = NumberEntity::adminSortImages($request);

        return view('admin.number.list_images', compact('number'))->render();
    }

    /**
     * Switch active image
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function imageSwitchActive(Request $request)
    {
        $number = NumberEntity::adminImageSwitchActive($request);

        return view('admin.number.list_images', compact('number'))->render();
    }

    /**
     * Delete image and thumbnail
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function removeImage(Request $request)
    {
        $number = NumberEntity::adminRemoveImage($request);

        return view('admin.number.list_images', compact('number'))->render();
    }

    /**
     * Add price
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addPrice(Request $request)
    {
        $numberCost = NumberEntity::adminAddPrice($request);

        return view('admin.number.new_price', compact('numberCost'));
    }

    /**
     * Remove price
     *
     * @param Request $request
     * @return string
     */
    public function removePrice(Request $request) :string
    {
        return NumberEntity::adminRemovePrice($request);
    }

    /**
     * Switch active number
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function switchActive(Request $request)
    {
        $numbers = NumberEntity::adminSwitchActive($request);

        return view('admin.number.list_numbers', compact('numbers'))->render();
    }

    /**
     * Sort numbers
     *
     * @param Request $request
     */
    public function sortNumbers(Request $request)
    {
        NumberEntity::adminSortNumbers($request);
    }

}
