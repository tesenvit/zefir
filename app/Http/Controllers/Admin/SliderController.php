<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Entities\SliderEntity;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * Show slider page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $slides = Slider::getSlidesSortBy('desktop');
        $slidesMobile = Slider::getSlidesSortBy('mobile');

        return view('admin.sliders.index', compact('slides','slidesMobile'));
    }

    /**
     * Save image
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function saveImage(Request $request)
    {
        SliderEntity::adminSaveImage($request);

        $type = $request->type;
        $slides = Slider::getSlidesSortBy('desktop');
        $slidesMobile = Slider::getSlidesSortBy('mobile');

        return view('admin.sliders.list_slides', compact('type','slides','slidesMobile'))->render();
    }

    /**
     * Sort images
     *
     * @param Request $request
     */
    public function sortImages(Request $request) :void
    {
        SliderEntity::adminSortImages($request);
    }

    /**
     * Delete image
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function removeImage(Request $request)
    {
        SliderEntity::adminRemoveImage($request);

        $type = $request->type;
        $slides = Slider::getSlidesSortBy('desktop');
        $slidesMobile = Slider::getSlidesSortBy('mobile');

        return view('admin.sliders.list_slides', compact('type','slides','slidesMobile'))->render();
    }

    /**
     * Switch active
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function switchActive(Request $request)
    {
        SliderEntity::adminSwitchActive($request);

        $type = $request->type;
        $slides = Slider::getSlidesSortBy('desktop');
        $slidesMobile = Slider::getSlidesSortBy('mobile');

        return view('admin.sliders.list_slides', compact('type','slides','slidesMobile'))->render();
    }
}
