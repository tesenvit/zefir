<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Entities\GalleryEntity;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    /**
     * Show gallery
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $gallery = Gallery::getBySort();

        return view('admin.gallery.index', compact('gallery'));
    }

    /**
     * Save image
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function saveImage(Request $request)
    {
        GalleryEntity::adminSaveImage($request);

        $gallery = Gallery::getBySort();

        return view('admin.gallery.list_images', compact('gallery'))->render();
    }

    /**
     * Sort images
     *
     * @param Request $request
     */
    public function sortImages(Request $request) :void
    {
        GalleryEntity::adminSortImages($request);
    }

    /**
     * Switch active
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function switchActive(Request $request)
    {
        GalleryEntity::adminSwitchActive($request);

        $gallery = Gallery::getBySort();

        return view('admin.gallery.list_images', compact('gallery'))->render();
    }

    /**
     * Delete image with thumbnail
     *
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function removeImage(Request $request)
    {
        GalleryEntity::adminRemoveImage($request);

        $gallery = Gallery::getBySort();

        return view('admin.gallery.list_images', compact('gallery'))->render();
    }

    public function enableNumbersPhoto()
    {
        $current = settings('gallery_enable_numbers_photo');
        settings(['gallery_enable_numbers_photo' => $current ? 0 : 1]);

        return view('admin.gallery.enable_numbers_photo')->render();
    }

}
