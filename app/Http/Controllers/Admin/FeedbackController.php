<?php

namespace App\Http\Controllers\Admin;

use App\Entities\FeedbackEntity;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    /**
     * Show all feedback
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $feedback = FeedbackEntity::getAll();

        return view('admin.feedback.index', compact('feedback'));
    }
}
