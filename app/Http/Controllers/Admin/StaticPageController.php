<?php

namespace App\Http\Controllers\Admin;

use App\Models\StaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{
    /**
     * Show static page
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $slug)
    {
        $page = StaticPage::where('slug', $slug)->first();

        return view('admin.static_page.edit', compact('page'));
    }

    /**
     * Updated static page
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $page = StaticPage::find($request->id);

        $page->update($request->all());

        return redirect()->back()->with('success', 'Данные успешно обновлены!');
    }
}
