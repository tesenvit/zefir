<?php

namespace App\Http\Controllers\Front;

use App\Models\Number;
use App\Models\Slider;
use App\Http\Controllers\Controller;

class MainPageController extends Controller
{
    /**
     * Show main page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $slidesDesktop = Slider::getSlidesActive('desktop');
        $slidesMobile = Slider::getSlidesActive('mobile');
        $numbers = Number::where('active', 1)->orderBy('sort', 'asc')->get();

        return view('front.main_page.index', compact('slidesDesktop','slidesMobile', 'numbers'));
    }
}
