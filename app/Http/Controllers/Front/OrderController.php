<?php

namespace App\Http\Controllers\Front;

use App\Entities\OrderEntity;
use App\Http\Controllers\Controller;
use App\Http\Requests\Front\OrderReserveRequest;

class OrderController extends Controller
{
    private $msgBeforeOrder = 'Спасибо за заказ. Мы с вами свяжемся в ближайшее время!';

    /**
     * Store new order
     *
     * @param OrderReserveRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reserve(OrderReserveRequest $request)
    {
        OrderEntity::createNewOrder($request);

        return redirect()->back()->with('successModal', $this->msgBeforeOrder);
    }
}

