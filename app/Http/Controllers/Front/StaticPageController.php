<?php

namespace App\Http\Controllers\Front;

use App\Models\StaticPage;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{
    /**
     * Show about page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        $aboutPage = StaticPage::where('slug', 'about')->first();

        return view('front.static_pages.about', compact('aboutPage'));
    }

    /**
     * Show contacts page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contacts()
    {
        $contactsPage = StaticPage::where('slug', 'contacts')->first();

        return view('front.static_pages.contacts', compact('contactsPage'));
    }
}
