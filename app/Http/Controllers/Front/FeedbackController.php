<?php

namespace App\Http\Controllers\Front;

use App\Entities\FeedbackEntity;
use App\Http\Controllers\Controller;
use App\Http\Requests\Front\FeedbackRequest;

class FeedbackController extends Controller
{
    /**
     * Store new feedback
     *
     * @param FeedbackRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FeedbackRequest $request)
    {
        FeedbackEntity::frontStore($request);

        $message = 'Спасибо за ваш отзыв. Мы свяжемся с вами в ближайшее время';

        return redirect()->back()->with('successModal', $message);
    }
}
