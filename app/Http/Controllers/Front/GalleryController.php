<?php

namespace App\Http\Controllers\Front;

use App\Models\Number;
use App\Models\Gallery;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    /**
     * Show gallery images and images of numbers
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $gallery = Gallery::getBySortWithActive();
        $numbers = Number::getBySortWithActive();

        return view('front.gallery.show', compact('gallery', 'numbers'));
    }
}
