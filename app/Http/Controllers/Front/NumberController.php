<?php

namespace App\Http\Controllers\Front;

use App\Entities\NumberEntity;
use App\Http\Controllers\Controller;

class NumberController extends Controller
{
    /**
     * Show number
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $number = NumberEntity::frontGetBySlug($slug);

        return view('front.number.show', compact('number'));
    }
}
