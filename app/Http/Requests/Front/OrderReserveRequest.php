<?php

namespace App\Http\Requests\Front;

use App\Rules\DatePeriod;
use Illuminate\Foundation\Http\FormRequest;

class OrderReserveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|regex:/^\+[0-9]{8,14}$/',
            'adult' => 'required|numeric',
            'children' => 'required|numeric',
            'number_type' => 'required|exists:numbers,id',
            'date' => [
                'required',
                'regex:/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}-[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/',
                new DatePeriod()
            ],
            'terms' => 'required',
            'comment' => 'nullable|min:2',
        ];
    }


    /**
     * Gives out where to redirect
     *
     * @return string
     */
    protected function getRedirectUrl() :string
    {
        $typeFrom = $this->request->get('typeForm');

        $url = $this->redirector->getUrlGenerator();

        switch ($typeFrom)
        {
            case 'modal':
                session()->flash('bookingModalCheck', 1);
                return $url->previous();
                break;

            case 'number':
            case 'mainPage':
                return $url->previous() . '#bookingform';
                break;

            default:
                return $url->previous();
        }
    }
}
