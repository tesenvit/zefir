<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NumberUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'preview' => 'required',
            'description' => 'required',
            'dates' => 'required',
            'prices' => 'required'
        ];

        if($this->request->get('dates') && $this->request->get('prices'))
        {
            foreach($this->request->get('dates') as $key => $val)
            {
                $rules['dates.'.$key] = 'required|regex:/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}(-[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4})?$/';
            }

            foreach($this->request->get('prices') as $key => $val)
            {
                $rules['prices.'.$key] = 'required';
            }
        }

        return $rules;
    }

}
