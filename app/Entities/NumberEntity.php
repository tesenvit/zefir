<?php

namespace App\Entities;

use Carbon\Carbon;
use App\Models\Number;
use Carbon\CarbonPeriod;
use App\Models\NumberCost;
use Illuminate\Support\Str;
use App\Models\NumberImage;
use Illuminate\Http\Request;
use App\Components\Imageable;
use Illuminate\Support\Facades\Storage;

class NumberEntity
{
    use Imageable;

    /**
     *  Store new number
     *
     * @param Request $request
     */
    public static function adminStore(Request $request) :void
    {
        Number::create([
            'title' => $request->title,
            'preview' => $request->preview,
            'description' => $request->description,
            'sort' => 0,
            'active' => 0,
            'slug' => Str::slug($request->title)
        ]);
    }

    /**
     * Update data of number
     *
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public static function adminUpdate(Request $request, string $id)
    {
        if(!self::dateValidate($request->dates))
        {
            return redirect()->back()->withErrors(['dates'=> ['Даты не должны пересекаться']])->withInput();
        }

        self::saveDescriptionData($request, $id);

        self::saveDates($request->dates);

        self::savePrices($request->prices);
    }

    /**
     * Check recurring dates
     *
     * @param array $allDatesOfNumbers
     * @return bool
     */
    private static function dateValidate(array $allDatesOfNumbers) :bool
    {
        $range = collect([]);

        foreach ($allDatesOfNumbers as $dates)
        {
            $date = self::makeDateFormat($dates);

            $period = CarbonPeriod::create($date['from'], $date['to']);

            foreach ($period as $date)
            {
                $range->push($date->format('d.m.Y'));
            }
        }

        $checkRepeat = $range->diffAssoc($range->unique());

        return ($checkRepeat->isEmpty()) ? true : false;
    }

    /**
     * Make date with Carbon
     *
     * @param string $period
     * @return array
     */
    private static function makeDateFormat(string $period) :array
    {
        $date = explode('-', $period);

        if(!isset($date[1]))
            $date[1] = $date[0];

        return [
            'from' => Carbon::parse($date[0]),
            'to' => Carbon::parse($date[1])
        ];
    }

    /**
     * Save description data as title, desc, preview
     *
     * @param Request $request
     * @param string $id
     */
    private static function saveDescriptionData(Request $request, string $id) :void
    {
        $number = Number::find($id);

        $number->title = $request->title;
        $number->description = $request->description;
        $number->preview = $request->preview;
        $number->slug = Str::slug($request->title);

        $number->save();
    }

    /**
     * Save dates
     *
     * @param array $dates
     */
    private static function saveDates(array $dates) :void
    {
        foreach ($dates as $id => $period)
        {
            $date = self::makeDateFormat($period);

            $numberCost = NumberCost::find($id);
            $numberCost->from = $date['from'];
            $numberCost->to = $date['to'];

            $numberCost->save();
        }
    }

    /**
     * Save prices
     *
     * @param array $prices
     */
    private static function savePrices(array $prices) :void
    {
        foreach ($prices as $id => $price)
        {
            $numberCost = NumberCost::find($id);
            $numberCost->price = $price;
            $numberCost->save();
        }
    }

    /**
     * Delete number with relations and images
     *
     * @param string $id
     */
    public static function adminDestroy(string $id) :void
    {
        $number = Number::find($id);

        foreach ($number->images as $image)
        {
            Storage::disk('public')->delete('numbers/' . $image->path);
            Storage::disk('public')->delete('numbers/thumbnails/' . $image->thumbnail);
        }

        $number->images()->delete();
        $number->cost()->delete();
        $number->delete();
    }

    /**
     * Make image with thumbnail and save them
     *
     * @param Request $request
     * @return mixed
     */
    public static function adminSaveImage(Request $request)
    {
        if ($request->hasFile('image'))
        {
            $file = $request->file('image');

            $path = Imageable::make($file, 'app/public/numbers/', 1920 , 80);

            $pathThumbnail = Imageable::make($file, 'app/public/numbers/thumbnails/', 400 , 80);

            $number = Number::find($request->id);

            $number->images()->create([
                'path' => $path,
                'thumbnail' => $pathThumbnail,
                'sort' => 0,
                'active' => 1
            ]);

            return $number;
        }
    }

    /**
     * Sort images with drag and drop
     *
     * @param Request $request
     * @return mixed
     */
    public static function adminSortImages(Request $request)
    {
        if($request->has('ids'))
        {
            $number = Number::find($request->numberId);

            foreach ($request->ids as $sort => $id) {
                $number->images()->find($id)->update([
                    'sort' => $sort
                ]);
            }

            return $number;
        }
    }

    /**
     * Switch active
     *
     * @param Request $request
     * @return mixed
     */
    public static function adminImageSwitchActive(Request $request)
    {
        if($request->has('id'))
        {
            $image = NumberImage::find($request->id);

            $image->update(['active' => $image->active ? 0 : 1]);

            return $image->number;
        }
    }

    /**
     * Delete image and thumbnail
     *
     * @param Request $request
     * @return mixed
     */
    public static function adminRemoveImage(Request $request)
    {
        if($request->has('id'))
        {
            $image = NumberImage::find($request->id);

            Storage::disk('public')->delete('numbers/' . $image->path);
            Storage::disk('public')->delete('numbers/thumbnails/' . $image->thumbnail);

            $image->delete();

            return $image->number;
        }
    }

    /**
     * Add price
     *
     * @param Request $request
     * @return mixed
     */
    public static function adminAddPrice(Request $request)
    {
        $number = Number::find($request->numberId);

        return $number->cost()->create();
    }

    /**
     * Remove price
     *
     * @param Request $request
     * @return string
     */
    public static function adminRemovePrice(Request $request) :string
    {
        $cost = NumberCost::find($request->id);

        $cost->delete();

        return $request->id;
    }

    /**
     * Switch active number
     *
     * @param Request $request
     * @return mixed
     */
    public static function adminSwitchActive(Request $request)
    {
        if($request->has('id'))
        {
            $number = Number::find($request->id);

            $number->update(['active' => $number->active ? 0 : 1]);

            return Number::orderBy('sort', 'asc')->get();
        }
    }

    /**
     * Sorting numbers
     *
     * @param Request $request
     */
    public static function adminSortNumbers(Request $request) :void
    {
        if($request->has('ids'))
        {
            foreach ($request->ids as $sort => $id)
            {
                Number::find($id)->update([
                    'sort' => $sort
                ]);
            }
        }
    }

    /**
     * Get Number by slug
     *
     * @param string $slug
     * @return mixed
     */
    public static function frontGetBySlug(string $slug)
    {
        $number = Number::where('slug', $slug)->first();

        if($number)
        {
            return $number;
        }
        else
        {
            abort(404);
        }
    }

}
