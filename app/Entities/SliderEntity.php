<?php

namespace App\Entities;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Components\Imageable;
use Illuminate\Support\Facades\Storage;

class SliderEntity
{
    use Imageable;

    /**
     * Save image
     *
     * @param Request $request
     */
    public static function adminSaveImage(Request $request)
    {
        if ($request->hasFile('image'))
        {
            $path = Imageable::make($request->file('image'), 'app/public/slider/', 1920 , 80);

            Slider::create([
                'type' => $request->type,
                'path' => $path,
                'sort' => 0,
                'active' => 1
            ]);
        }
    }

    /**
     * Sort images
     *
     * @param Request $request
     */
    public static function adminSortImages(Request $request) :void
    {
        if($request->has('ids'))
        {
            foreach ($request->ids as $sort => $id)
            {
                Slider::find($id)->update(['sort' => $sort]);
            }
        }
    }

    /**
     * Delete image
     *
     * @param Request $request
     */
    public static function adminRemoveImage(Request $request) :void
    {
        if($request->has('id'))
        {
            $slide = Slider::find($request->id);

            Storage::disk('public')->delete('slider/' . $slide->path);

            $slide->delete();
        }
    }

    /**
     * Switch active
     *
     * @param Request $request
     */
    public static function adminSwitchActive(Request $request) :void
    {
        if($request->has('id'))
        {
            $slide = Slider::find($request->id);

            $slide->update(['active' => $slide->active ? 0 : 1]);
        }
    }
}