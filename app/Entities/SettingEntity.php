<?php

namespace App\Entities;

use App\Models\Settings;

class SettingEntity
{
    /**
     * Get value by key
     *
     * @param $key
     * @return string|null
     */
    public static function getValue($key) :?string
    {
        $key = Settings::where('key', $key)->first();

        $result = null;

        if($key)
        {
            $result = $key->value;
        }

        return $result;
    }

    /**
     * Set values
     *
     * @param array $data
     */
    public static function setValue(array $data)
    {
        foreach ($data as $key => $value)
        {
            Settings::updateOrCreate(
                [
                    'key' => $key
                ],
                [
                    'key' => $key,
                    'value' => $value
                ]);
        }
    }
}