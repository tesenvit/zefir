<?php

namespace App\Entities;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Number;
use App\Mail\OrderShipped;
use App\Components\TelegramBot;
use Illuminate\Support\Facades\Mail;

class OrderEntity
{
    const PAGINATE_ITEMS = 15;

    public static function createNewOrder($request)
    {
        $order = self::frontStore($request);
        self::sendNotifications($order);
    }

    /**
     * Store order
     *
     * @param $request
     * @return Order
     */
    private static function frontStore($request) :Order
    {
        $dates  = explode('-', $request->date);
        $number = Number::find($request->number_type);

        return Order::create([
            'name'         => $request->name,
            'phone'        => $request->phone,
            'adult'        => $request->adult,
            'children'     => $request->children,
            'number_id'    => $request->number_type,
            'number_title' => $number->title,
            'sum'          => $number->getSumByPeriod($dates),
            'comment'      => $request->comment,
            'from'         => Carbon::create($dates[0]),
            'to'           => Carbon::create($dates[1]),
            'viewed'       => 0,
        ]);
    }


    /**
     * Send notification to telegram and mail
     *
     * @param Order $order
     */
    public static function sendNotifications(Order $order) :void
    {
        TelegramBot::sendOrderNotification($order);

        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new OrderShipped($order));
    }

    /**
     * Get all orders with paginate
     *
     * @param int $paginate
     * @return mixed
     */
    public static function adminGetAll($paginate = self::PAGINATE_ITEMS)
    {
        return Order::orderBY('id', 'desc')->paginate($paginate);
    }

    /**
     * Show order in admin panel
     *
     * @param $id
     * @return mixed
     */
    public static function adminShow($id)
    {
        $order = Order::find($id);

        self::writeViewed($order);

        return $order;
    }

    /**
     * Rewrite field viewed id this zero or null
     *
     * @param Order $order
     */
    private static function writeViewed(Order $order) :void
    {
        $viewed = $order->viewed;

        if(!$viewed)
        {
            $order->viewed = 1;
            $order->save();
        }
    }
}
