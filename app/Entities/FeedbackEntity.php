<?php

namespace App\Entities;

use App\Models\Feedback;

class FeedbackEntity
{
    /**
     * Store new feedback
     *
     * @param $request
     */
    static public function frontStore($request) :void
    {
        Feedback::create($request->all());
    }


    /**
     * Get all feedback
     *
     * @return mixed
     */
    static public function getAll()
    {
        self::writeView();

        return Feedback::orderBy('id', 'desc')->get();
    }

    /**
     * Check viewed feedback
     *
     * @return void
     */
    private static function writeView() :void
    {
        $feedbackNotView = Feedback::where('viewed', 0)->get();

        foreach ($feedbackNotView as $feedback)
        {
            $feedback->viewed = 1;
            $feedback->save();
        }
    }
}