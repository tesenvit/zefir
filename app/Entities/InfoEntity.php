<?php

namespace App\Entities;

use App\Models\Info;

class InfoEntity
{
    private static $_instance = null;

    /**
     * Get instance of singleton
     *
     * @return InfoEntity
     */
    public static function getInstance()
    {
        if (self::$_instance != null)
        {
            return self::$_instance;
        }

        return new self;
    }

    /**
     * Get array phones
     *
     * @return array
     */
    public function phones() :array
    {
        $phones = Info::where('key', 'phones')->first()->value;

        return preg_split("/[\s;]+/", $phones);
    }

    /**
     * Get rate USD dollar
     *
     * @return float
     */
    public function rate() :float
    {
        $rate = Info::where('key', 'rate')->first()->value;

        return str_replace(',','.', $rate);
    }

    /**
     * If there is no method, we get the value from the database with the key
     *
     * @param $name
     * @param $arguments
     * @return string|null
     */
    public function __call($name, $arguments) :?string
    {
        $value = Info::where('key', $name)->first();

        return $value ? $value->value : null;
    }

    private function __clone () {}
    private function __wakeup () {}
}