<?php

namespace App\Entities;

use App\Models\Text;

class TextEntity
{
    private static $_instance = null;

    /**
     * Get instance of singleton
     *
     * @return TextEntity
     */
    public static function getInstance()
    {
        if (self::$_instance != null)
        {
            return self::$_instance;
        }

        return new self;
    }

    /**
     * If there is no method, we get the value from the database with the key
     *
     * @param $name
     * @param $arguments
     * @return string|null
     */
    public function __call($name, $arguments) :?string
    {
        $value = Text::where('key', $name)->first();

        return $value ? $value->value : null;
    }

    private function __clone () {}
    private function __wakeup () {}
}