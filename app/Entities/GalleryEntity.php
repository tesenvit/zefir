<?php

namespace App\Entities;

use App\Models\Gallery;
use App\Components\Imageable;
use Illuminate\Support\Facades\Storage;

class GalleryEntity
{
    /**
     * Make image with thumbnail and save them
     *
     * @param $request
     */
    public static function adminSaveImage($request) :void
    {
        if ($request->hasFile('image'))
        {
            $file = $request->file('image');

            $path = Imageable::make($file, 'app/public/gallery/', 1920 , 80);

            $pathThumbnail = Imageable::make($file, 'app/public/gallery/thumbnails/', 400 , 80);

            Gallery::create([
                'path' => $path,
                'thumbnail' => $pathThumbnail,
                'sort' => 0,
                'active' => 1
            ]);
        }
    }

    /**
     * Sort images
     *
     * @param $request
     */
    public static function adminSortImages($request) :void
    {
        if($request->has('ids'))
        {
            foreach ($request->ids as $sort => $id)
            {
                Gallery::find($id)->update(['sort' => $sort]);
            }
        }
    }

    /**
     * Switch active
     *
     * @param $request
     */
    public static function adminSwitchActive($request) :void
    {
        if($request->has('id'))
        {
            $image = Gallery::find($request->id);

            $image->update(['active' => $image->active ? 0 : 1]);
        }
    }

    /**
     * Delete image with thumbnails
     *
     * @param $request
     */
    public static function adminRemoveImage($request) :void
    {
        if($request->has('id'))
        {
            $image = Gallery::find($request->id);

            Storage::disk('public')->delete('gallery/' . $image->path);
            Storage::disk('public')->delete('gallery/thumbnails/' . $image->thumbnail);

            $image->delete();
        }
    }
}