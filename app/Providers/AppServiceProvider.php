<?php

namespace App\Providers;

use App\Models\Number;
use App\Models\StaticPage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('recaptcha', 'App\\Components\\ReCaptcha@validate');

        /* Numbers for menu on front pages*/
        $this->shareNumbersForFrontMenu();

        /* Static page rules for front */
        $this->shareStaticPageRulesForFront();

        /* Static pages for menu on admin panel*/
        $this->shareStaticPagesForAdmin();
    }

    private function shareNumbersForFrontMenu()
    {
        $numbers = Number::getBySortWithActive();

        $this->makeViewWith($numbers,'allNumbers','front');
    }

    private function shareStaticPagesForAdmin()
    {
        $staticPages = StaticPage::all();

        $this->makeViewWith($staticPages,'adminStaticPages','admin');
    }

    private function shareStaticPageRulesForFront()
    {
        $rules = StaticPage::where('slug','rules')->first();

        $this->makeViewWith($rules,'rules','front');
    }

    private function makeViewWith($data, string $name, string $layout)
    {
        view()->composer('layouts.' . $layout . '.app', function($view) use($data, $name)
        {
            $view->with($name, $data);
        });
    }
}
