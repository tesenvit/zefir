<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumberImage extends Model
{
    protected $fillable = [
        'number_id', 'path', 'thumbnail', 'sort', 'active'
    ];

    public function number()
    {
        return $this->belongsTo('App\Models\Number');
    }
}
