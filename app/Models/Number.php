<?php

namespace App\Models;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $fillable = [
        'title', 'description', 'sort', 'count', 'preview', 'active', 'slug'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\NumberImage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cost()
    {
        return $this->hasMany('App\Models\NumberCost');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getMainImage()
    {
        return $this->images()->where('sort', 0)->first();
    }

    /**
     * @param string $type
     * @return mixed|string|null
     */
    public function getMainImagePath(string $type = 'original')
    {
        $image = $this->images()->where('sort', 0)->first();

        $path = null;

        if($image)
        {
            switch ($type)
            {
                case 'original':
                    $path = $image->path;
                    break;

                case 'thumbnail':
                    $path = 'thumbnails/' . $image->thumbnail;
                    break;
            }
        }
        else
        {
            $path = 'default.jpeg';
        }

        return $path;
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function getPrice($date = null)
    {
        $desiredDate = $date ?? Carbon::now();

        foreach ($this->cost as $cost)
        {
            $period = CarbonPeriod::create(Carbon::parse($cost->from), Carbon::parse($cost->to));

            foreach ($period as $date)
            {
                if($date->format('d.m.Y') == $desiredDate->format('d.m.Y'))
                {
                   return $cost->price;
                }
            }
        }
    }

    /**
     * @param array $dates
     * @return int
     */
    public function getSumByPeriod(array $dates)
    {
        $sum    = 0;
        $period = CarbonPeriod::create(Carbon::parse($dates[0]), Carbon::parse($dates[1]));

        foreach ($period as $date)
        {
           $sum += $this->getPrice($date);
        }

        return $sum;
    }

    /**
     * @return mixed
     */
    public static function getBySortWithActive()
    {
        return self::where('active', 1)->orderBy('sort', 'asc')->with('images')->get();
    }
}
