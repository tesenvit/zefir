<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';

    protected $fillable = [
        'path', 'thumbnail', 'active' ,'sort'
    ];

    public static function getBySort()
    {
        return self::orderBy('sort', 'asc')->get();
    }

    public static function getBySortWithActive()
    {
        return self::where('active', 1)->orderBy('sort', 'asc')->get();
    }
}
