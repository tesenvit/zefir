<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'name', 'phone', 'adult', 'children', 'number_id', 'number_title', 'from', 'to', 'viewed', 'comment', 'sum'
    ];

    protected $dates = ['from', 'to', 'created_at', 'updated_at'];
}
