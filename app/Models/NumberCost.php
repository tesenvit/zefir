<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumberCost extends Model
{
    protected $fillable = [
        'number_id', 'from', 'to', 'price'
    ];

    protected $dates = ['from', 'to', 'created_at', 'updated_at'];

    public function number()
    {
        return $this->belongsTo('App\Models\Number');
    }
}
