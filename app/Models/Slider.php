<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'type', 'path', 'sort', 'active'
    ];

    public static function getSlidesSortBy($type)
    {
        return self::where('type', $type)->orderBy('sort', 'asc')->get();
    }

    public static function getSlidesActive($type)
    {
        return self::where('type', $type)->where('active', 1)->orderBy('sort', 'asc')->get();
    }
}
